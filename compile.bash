# Compiles four examples:
# OneMax problem
# MaxSAT problem
# TSP problem
# Protein Folding in a 2D square lattice HP model problem
#
# Please make sure that the directory for sourcepath and the java files (in fully qualified names) are compatible,
# that is, this file (compile.bash) must be on the same (parent) directory as the MAFRA framework.
#

javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/OneMaxTest.java
javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/MaxSATTest.java
javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/TSPTestElitistGAMuPlusLambda.java
javac -g -sourcepath . -d ../tmp/classes/ MAFRA/Test/MultiMemePF2DSquareRel.java

# Compiled *.class files are placed under the ../tmp/classes/ directory.
#
# To run, e.g., OneMaxTest.class go to directory ../tmp/ and type in the command line:
# java -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.OneMaxTest
#
# Other executables may require additional arguments, e.g.:
#java  -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.MaxSATTest ~/PROBLEM_INSTANCES/SAT/uf50-01.cnf
#java  -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.TSPTestElitistGAMuPlusLambda ~/PROBLEM_INSTANCES/TSP/eil51.tsp TEST 12345 1000 1000 true
#java  -cp ./classes/DataStructures:./classes/Exceptions:./classes/Supporting:./classes MAFRA.Test.MultiMemePF2DSquareRel HPHPPHHPHPPHPHHPPHPH TEST 12345 100 100 10 true true 0.10 5 1 true 0.25 0.7 100 true
