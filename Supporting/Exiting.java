package Supporting;

/**
 * Class that contains a patch for Cafe.
 */
public class Exiting
{
    /**
     * Method used to pause the program for a long time.
     * It is useful because console programs that run under
     * Cafe destroy the console upon termination,
     * making it hard to see the output.
     */
    public static void longPause( )
    {
        try
          { Thread.sleep( 1000000 ); }
        catch( InterruptedException e )
          { }
    }

    /**
     * Prompt for input from the keyboard, and
     * then exit the application.
     */
    public static void promptToExit( )
    {
        System.out.println( "Press the ENTER key to exit" );
        try
          { System.in.read( ); }
        catch( java.io.IOException e ) { }

        System.exit( 0 );
    }
}