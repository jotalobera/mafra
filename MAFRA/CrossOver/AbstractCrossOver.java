
package MAFRA.CrossOver;


/** Just a Memebrship class to encompass all AbstractCrossover classes.
@version Memetic Algorithms Framework - V 1.0 - July 1999
@author  Natalio Krasnogor
*/

public abstract class AbstractCrossOver {

  public void execute()
    {};

  public Object twoParentsCrossOver(Object parent1,Object parent2)
    {
      return null;
    }

  public Object threeParentsCrossOver(Object parent1,Object parent2,Object parent3)
    {
      return null;
    }

  public Object fourParentsCrossOver(Object parent1,Object parent2,Object parent4)
    {
      return null;
    }



}
