

package MAFRA.CrossOver;

import java.util.BitSet;
import java.io.*; 
import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** @version  Behaviour Based Uniform Crossover (BitSetBehaviourUX) V1.0 - September 1999. Contribution to MAFRA. Adapted to MAFRa by Natalio Krasnogor
    @author  Pablo Moscato
 */

/* The 5 comments bellow are to be ignored for standalone MAFRA users */

// if you are using this without the MemePool software
// you should remove the comment mark of the line below
// package Contrib.MemePool.Recombinations.BitSet

// otherwise at MemePool, this is part of package...
// package MemePool.Recombinations.BitSet

public class BitSetBehaviourUX extends AbstractBitSetCrossOver{

 public Object twoParentsCrossOver(Object parent1, Object parent2)
 
    {
      BitSet p1;                              // from one parent 
      BitSet p2;                              // from the other parent
      BitSet childChromosome; 
      BitSetIndividual child;        // the invididual we will create
      double rndValue;                // used for some random choices 
      double rndBehaviour;        // to select a random behaviour
      int    i;                                  // just to iterate
      int    size;                            // size of the BitSet
       
      // System.out.println("You are using Behaviour-based recombination !");
      
      // assign the two chromosomes of the parent solutions to be recombined
      // to logical names... 
      if( (((Individual)parent1).getFitness()) >= (((Individual)parent2).getFitness()) ){
        p1 = (BitSet)(((Individual)parent1).getChromosome());
        p2 = (BitSet)(((Individual)parent2).getChromosome());
        }
      else{
        p2 = (BitSet)(((Individual)parent1).getChromosome());
        p1 = (BitSet)(((Individual)parent2).getChromosome());
        } // so p1 is the chromosome of the better adapted parent
      // the size of the child will be the same of the size of the parents
      size =(int)( ((Individual)parent1).getSize());
      // construct the child as a BitSet
      childChromosome = new BitSet(size); 
      
      // select a behaviour uniformly at random with equal probability
      rndBehaviour=MAFRA_Random.nextUniformDouble(0.0,1.0);
      if (rndBehaviour<0.1){
        //System.out.println(rndBehaviour);
        for (i=0;i<size;i++) {
             //System.out.println(i);
             rndValue = MAFRA_Random.nextUniformDouble(0.0,1.0);        
             if(rndValue<0.333333){
               //System.out.println(rndValue);
               if(p1.get(i)) {childChromosome.set(i);}
               else            {childChromosome.clear(i);}
              } 
             else{ 
               //System.out.println(rndValue);
               if(p2.get(i)) {childChromosome.set(i);}
               else             {childChromosome.clear(i);}}
             } // end of for loop for conciliator behaviour
        } // this was the conciliator behaviour. Same as UX
      else{ // now the rebel and obsequent cases
        for (i=0;i<size-1;i++) { // again a loop
           if ((p1.get(i))!=(p2.get(i))){ // if the allele value differs
             if (rndBehaviour<0.6666666){ // rebel behaviour
               if(p2.get(i)) {childChromosome.set(i);}
               else            {childChromosome.clear(i);}
               }
             else{ // obsequent behaviour
               if(p1.get(i)){childChromosome.set(i);}
               else            {childChromosome.clear(i);}
             }
          }
          else{ // the two genes have the same allele value...
             // we decide uniformly at random
             rndValue = MAFRA_Random.nextUniformDouble(0.0,1.0);
             if (rndValue<0.5)   {childChromosome.set(i);} 
             else                        {childChromosome.clear(i);}
             // these values might be decided via a greedy assignment
            // thus suggesting other recombination procedures...
          }
	     
        } // end of for all allele values loop
      } // end of cases obsequent or rebel
     
      // create the new individual 
      child = new BitSetIndividual();
      child.setChromosome(childChromosome);
      // we don't know its fitness yet, and it is not where 
      // it should be calculated...
      child.setFitness(0.0); // but we set it to zero... 
      child.setSize(((Individual)parent1).getSize());    

    return  (Object) child; // returns a new individual !
  }
}
