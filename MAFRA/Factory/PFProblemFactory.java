package MAFRA.Factory;




import DataStructures.*;
import Exceptions.*;
import Supporting.*;

import java.util.StringTokenizer;
import java.io.*;
import java.awt.*;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


public abstract class PFProblemFactory extends ProblemFactory
{ 
  
 public PFProblemFactory(double aSize,String aName)
    {
	super(aSize,aName);
    }
  
  /** Generates a random substructure of a given length in the approapriate alphabet */
    public abstract String getSubstructure(long length);
 
  /** Generates a stretched substructure, in this alphabet means a substructure all with F */
    public abstract String getStretchSubstructure(long length);
 
 
  /** Generates a pivoted substructure by mutating one position in the string */
    public abstract String getPivotSubstructure(String aStructure, int where, int rLength, int pivotType);

  /** Generates a shifted substructures by moving the position of aminoacid i to aminoacid i +/- j
        for some j */
   public abstract String getShiftSubstructure(String aStructure, int where, int length);

 
    /** Generates a reflected substructure on one of the simetry axes*/
   public abstract String getReflectSubstructure(String aStructure, int where, int rLength, int reflectType); 



}


