
package MAFRA.Factory;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


/** This factory is used to initialize a population with Individuals whose
Chromosome is a tour and a meme that is a helper.
 @version Memetic Algorithms Framework - V 1.2 - August 2000
 @author  Natalio Krasnogor
 */
public class SelfAdaptingStaticIndividualPFStringFactory extends IndividualFactory{

  protected ProblemFactory myProblem;
  protected int helpersNumber=0;

  public SelfAdaptingStaticIndividualPFStringFactory(ProblemFactory myP)
    {
      myProblem = myP;
      helpersNumber = 0;
    }

 public SelfAdaptingStaticIndividualPFStringFactory(ProblemFactory myP, int numHelps)
    {
      myProblem = myP;
      helpersNumber = numHelps;
    }




  public Individual createNewIndividual()
    {
      SelfAdaptingStaticPFStringIndividual aI;

      aI = new SelfAdaptingStaticPFStringIndividual(myProblem);
      if(helpersNumber>1)
      {
       aI.setHelperId((int)(MAFRA_Random.nextUniformLong(0,helpersNumber)));
      }
      else
      {
       aI.setHelperId(0);
      }
      return aI;
    }
}
      
 







