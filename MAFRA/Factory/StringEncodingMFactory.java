

package MAFRA.Factory;

import MAFRA.Mutation.*;
import MAFRA.Util.StringData;


/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public class StringEncodingMFactory extends MutationFactory implements StringData{

  /* StringData fields */
  private char alphabet[];
  private long alphabetSize;
  private boolean variableLength; /* variableLength=1 if the strings to be represented will be of variable
                                 length, 0 otherwise */
  private long stringLength;
  private boolean mutateTwoConsecutiveAsOne;

  /* Implementation of the StringData methods */
  public void setAlphabetSize(long size)
    {
      alphabetSize = size;
    }

  public void setVariableLength(boolean variableL)
    {
      variableLength = variableL;
    }

  public void setStringLength(long lengthS)
    {
      stringLength = lengthS;
    }


  public void setSimbols(char simbols[])
    {
      int i;
          
      if(alphabetSize<=0)
      {
       System.out.println("ERROR - trying to assign an alphabet without specifying its size in StringEncodingMFactory.java\n");
       System.exit(0);
      }	  
      alphabet = new char[(int)alphabetSize];
      for(i=0;i<alphabetSize;i++)
	{
	  alphabet[i] = simbols[i];
	}
    }

  public long getAlphabetSize()
    {
      return alphabetSize;
    }





  public char []getAlphabet()
    {
	return  alphabet;
    }




  public boolean getVariableLength()
    {
      return variableLength;
    }

  public long getStringLength()
    {
      return stringLength;
    }

  public char getSimbol(long number)
    {
      return alphabet[(int)number];
    }




 public StringEncodingMFactory()
 {
  super();
  alphabetSize=0;

 }

 public StringEncodingMFactory(boolean twoConsecutiveGenes)
 {
  super();
  alphabetSize=0;
  mutateTwoConsecutiveAsOne = twoConsecutiveGenes;
 }
  

 public StringEncodingMFactory(PFProblem aP)
 {
  super();
  alphabetSize=aP.TRI2DABALPHABETSIZE;
  setSimbols(aP.TRI2DABALPHABET);
 }
 
 public  AbstractMutation createOnePointMutation()
   {
    StringOnePointMutation oPM;
    oPM = new StringOnePointMutation();
    if((alphabetSize<=0)||(alphabet==null))
    {
     System.out.println("ERROR - trying to createOnePointMutation without setting the alphabetSize and/or alphabet in StringEncodingMFactory");
     System.exit(0);
    }
    oPM.setAlphabetSize(alphabetSize);
    oPM.setSimbols(alphabet);
    oPM.mutateTwoConsecutiveAsOne(mutateTwoConsecutiveAsOne);
    return (oPM);
   };

 public  AbstractMutation createTwoPointMutation()
   {
  /*  StringTwoPointMutation tPM;
    tPM = new StringTwoPointMutation();
    tPM.setAlphabetSize(alphabetSize);
    tPM.setSimbols(alphabet);
    return(oPM);*/
    return null;
   };

 public  AbstractMutation createThreePointMutation()
   {
/*    StringThreePointMutation thPM;
    thPM = new StrinThreePointMutation();
    thPM.setAlphabetSize(alphabetSize);
    thPM.setSimbols(alphabet);
    return(thPM);*/
    return null;
   };

 public  AbstractMutation createFourPointMutation()
   {
/*    StringFourPointMutation fPM;
    fPM = new StringFourPointMutation();
    fPM.setAlphabetSize(alphabetSize);
    fPM.setSimbols(alphabet);
    return(fPM);*/
    return null;
   };


  
  

}
