
package MAFRA.Factory;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


/** This factory is used to initialize a population with Individuals whose
Chromosome is a BitSet with a meme which is the helper.
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class SelfAdaptingStaticIndividualBitSetFactory extends IndividualFactory{

  protected ProblemFactory myProblem;
  protected int            helpersNumber;

  public SelfAdaptingStaticIndividualBitSetFactory(ProblemFactory myP)
    {
      myProblem = myP;
      helpersNumber = 0;
    }



  public SelfAdaptingStaticIndividualBitSetFactory(ProblemFactory myP,int numHelps)
    {
      myProblem = myP;
      helpersNumber = numHelps;
    }

  public  Individual createNewIndividual()
    {
      SelfAdaptingStaticBitSetIndividual aI;

      aI = new SelfAdaptingStaticBitSetIndividual(myProblem);
      if(helpersNumber>1)
	{
	  aI.setHelperId((int)(MAFRA_Random.nextUniformLong(0,helpersNumber)));
	}
      else
	{
	  aI.setHelperId(0);
	}
      return aI;
    }
}
      
 
