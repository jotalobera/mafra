package MAFRA.Factory;




import DataStructures.*;
import Exceptions.*;
import Supporting.*;

import java.util.StringTokenizer;
import java.io.*;
import java.awt.*;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;


public class PFProblem extends PFProblemFactory implements StringData{

public static char TRI2DABALPHABET[]={'E','W','N','n','S','s'};
public static int  TRI2DABALPHABETSIZE=6;

private static TextArea TextAreaMsg;
private static Frame myFrame;
private String instance;
private char foldingSpace[][];
private int surfaceSize;
private int  structureLength;

 /* StringData fields */
  private char alphabet[];
  private long alphabetSize;
  private boolean variableLength; /* variableLength=1 if the strings to be represented will be of variable
                                 length, 0 otherwise */
  private long stringLength;

public PFProblem(String anInstance)
  {
    super(anInstance.length(),"Protein Structure Prediction");
    instance = new String(anInstance.toUpperCase());
     surfaceSize = (int)((size * 4) + 4);
     createFoldingSpace();
    alphabet     = TRI2DABALPHABET;
    alphabetSize = TRI2DABALPHABETSIZE;
    structureLength = (int)(size-1);


   }



  public double fitness(Individual anIndividual)
    {
     String positions;
     int posi[];
     int posj[];
     int k;
     int bonds = 0;
     int Nh = 0;
     int penalties = 0;
     int aux = 0;
     int i = (int) (2*size+1);
     int j = (int) (2*size+1);
     int scale = (int)(300*(1-size/14));
     int diam  = 10;


     positions =(String)( anIndividual.getChromosome());
     posi = new int[instance.length()];
     posj = new int[instance.length()];

     posi[aux] = i;
     posj[aux] = j;
     foldingSpace[i][j] = instance.charAt(0);
     // System.out.println("In PFProblem");
     ((SelfAdaptingStaticPFStringIndividual)anIndividual).drawFirstMonomer(i*10+scale, j*10+scale, foldingSpace[i][j]);
	if (instance.charAt(0) == 'H'){
		Nh++;
	}
//	System.out.println("("+posi[aux]+","+posj[aux]+") - Monomer: "+foldingSpace[i][j]+" - Bonds: "+bonds);

	for(k=1; k <= structureLength; k++){
		if (instance.charAt(k) == 'H'){
			Nh++;
		}
		switch (positions.charAt(k-1)){
			case 'E':	{
						j++;
						j++;
						if (foldingSpace[i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[i][j]=='P')||(foldingSpace[i][j]=='X'))
										{
										 foldingSpace[i][j] = instance.charAt(k);
										}
						if (foldingSpace[i][j] == 'H'){
							if(foldingSpace[i][j+2]=='H')
								bonds++;
							if(foldingSpace[i-1][j-1]=='H')
								bonds++;
							if(foldingSpace[i-1][j+1]=='H')
								bonds++;
							if(foldingSpace[i+1][j+1]=='H')
								bonds++;
							if(foldingSpace[i+1][j-1]=='H')
								bonds++;
						}
						break;
						}
			case 'W':	{
						j--;
						j--;
						if (foldingSpace[i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[i][j]=='P')||(foldingSpace[i][j]=='X'))
										{
										 foldingSpace[i][j] = instance.charAt(k);
										}
						if (foldingSpace[i][j] == 'H'){
							if(foldingSpace[i][j-2]=='H')
								bonds++;
							if(foldingSpace[i-1][j-1]=='H')
								bonds++;
							if(foldingSpace[i-1][j+1]=='H')
								bonds++;
							if(foldingSpace[i+1][j+1]=='H')
								bonds++;
							if(foldingSpace[i+1][j-1]=='H')
								bonds++;
						}
						break;
						}
			case 'N':	{
						i--;
						j--;
						if (foldingSpace[i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[i][j]=='P')||(foldingSpace[i][j]=='X'))
										{
										 foldingSpace[i][j] = instance.charAt(k);
										}
						if (foldingSpace[i][j] == 'H'){
							if(foldingSpace[i-1][j-1]=='H')
								bonds++;
							if(foldingSpace[i-1][j+1]=='H')
								bonds++;
							if(foldingSpace[i][j+2]=='H')
								bonds++;
							if(foldingSpace[i][j-2]=='H')
								bonds++;
							if(foldingSpace[i+1][j-1]=='H')
								bonds++;
						}
						break;
						}
			case 'n':	{
						i--;
						j++;
						if (foldingSpace[i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[i][j]=='P')||(foldingSpace[i][j]=='X'))
										{
										 foldingSpace[i][j] = instance.charAt(k);
										}
						if (foldingSpace[i][j] == 'H'){
							if(foldingSpace[i-1][j+1]=='H')
								bonds++;
							if(foldingSpace[i-1][j-1]=='H')
								bonds++;
							if(foldingSpace[i][j+2]=='H')
								bonds++;
							if(foldingSpace[i+1][j+1]=='H')
								bonds++;
							if(foldingSpace[i][j-2]=='H')
								bonds++;
						}
						break;
						}
			case 'S':	{
						i++;
						j++;
						if (foldingSpace[i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[i][j]=='P')||(foldingSpace[i][j]=='X'))
										{
										 foldingSpace[i][j] = instance.charAt(k);
										}
						if (foldingSpace[i][j] == 'H'){
							if(foldingSpace[i+1][j+1]=='H')
								bonds++;
							if(foldingSpace[i][j+2]=='H')
								bonds++;
							if(foldingSpace[i-1][j+1]=='H')
								bonds++;
							if(foldingSpace[i][j-2]=='H')
								bonds++;
							if(foldingSpace[i+1][j-1]=='H')
								bonds++;
						}
						break;
						}
			case 's':	{
						i++;
						j--;
						if (foldingSpace[i][j] != 'X'){
							penalties++;
						}
						if((foldingSpace[i][j]=='P')||(foldingSpace[i][j]=='X'))
										{
										 foldingSpace[i][j] = instance.charAt(k);
										}
						if (foldingSpace[i][j] == 'H'){
							if(foldingSpace[i+1][j-1]=='H')
								bonds++;
							if(foldingSpace[i-1][j-1]=='H')
								bonds++;
							if(foldingSpace[i+1][j+1]=='H')
								bonds++;
							if(foldingSpace[i][j-2]=='H')
								bonds++;
							if(foldingSpace[i][j+2]=='H')
								bonds++;
						}
						break;
						}
		}
		aux++;
		posi[aux] = i;
		posj[aux] = j;
           // System.out.println("In PFProblem");
	        ((SelfAdaptingStaticPFStringIndividual)anIndividual).drawMonomer(i*10+scale, j*10+scale, foldingSpace[i][j]);

	}
	bonds = bonds - (penalties * ((2 * Nh) + 3));
	for(int t = 0; t <= aux; t++)
		foldingSpace[posi[t]][posj[t]] = 'X';

	numberOfFitnessCalls+=1.0;
	return bonds;


    }

  public boolean feasibilityCheck(Individual anIndividual)
    {
    return true;
    };

// random fold
    public Object newSolution()
      {
       int i;
       int random;
       char  tmp[];
       int SIZE = structureLength;

       tmp = new char[SIZE];
       for (i=0;i<SIZE;i++)
       {
        tmp[i] =getSimbol((int)(MAFRA_Random.nextUniformLong(0,alphabetSize)));
       }

       return (new String(tmp));



      }


    private void createFoldingSpace(){
		foldingSpace = new char [surfaceSize][surfaceSize];
		for (int i = 0; i < surfaceSize; i++){
			for (int j = 0; j < surfaceSize; j++){
				foldingSpace [i][j] = 'X';
			}
		}
	}



  /* Implementation of the StringData methods */
  public void setAlphabetSize(long size)
    {
      alphabetSize = size;
    }

  public void setVariableLength(boolean variableL)
    {
      variableLength = variableL;
    }

  public void setStringLength(long lengthS)
    {
      stringLength = lengthS;
    }


  public void setSimbols(char simbols[])
    {
      int i;
      for(i=0;i<alphabetSize;i++)
	{
	  alphabet[i] = simbols[i];
	}
    }

  public long getAlphabetSize()
    {
      return alphabetSize;
    }




  public char []getAlphabet()
    {
	return  TRI2DABALPHABET;
    }


  public boolean getVariableLength()
    {
      return variableLength;
    }

  public long getStringLength()
    {
      return stringLength;
    }

  public char getSimbol(long number)
    {
      return alphabet[(int)number];
    }

  public String getSubstructure(long length)
  {
   char substr[];
   int rndSimb;
   int f;

   substr = new char[(int)length];
   for (f=0;f<length;f++)
   {
    rndSimb   = ((int)MAFRA_Random.nextUniformLong(0,getAlphabetSize()));
    substr[f]= getSimbol(rndSimb);
   }
  return (new String(substr));
 }


 public String getStretchSubstructure(long length)
  {
   char substr[];
   int rndSimb;
   int f;

   substr = new char[(int)length];
   rndSimb   = ((int)MAFRA_Random.nextUniformLong(0,getAlphabetSize()));
   for (f=0;f<length;f++)
   {
    substr[f]= getSimbol(rndSimb);
   }
  return (new String(substr));
 }


 public String getPivotSubstructure(String aStructure, int where, int rLength, int pivotType)
 {
  int i;
  String newStructure;
  char symbol;

  newStructure = aStructure.substring(0,where);

  for(i=where;i<(where+rLength);i++)
  {
   symbol=aStructure.charAt(i);
   switch(pivotType)
   {
    /*   {'E','W','N','n','S','s'} */
    case 1:
    {
     switch(symbol)
     {
      case 'E':
      {
       symbol='n';
       break;
      }
      case 'n':
      {
       symbol='N';
       break;
      }
      case 'N':
      {
       symbol='W';
       break;
      }
      case 'W':
      {
       symbol='s';
       break;
      }
      case 's':
      {
       symbol='S';
       break;
      }
      case 'S':
      {
       symbol='E';
       break;
      }
     }
     break;
    }
    case 2:
    {
    switch(symbol)
     {
      case 'E':
      {
       symbol='N';
       break;
      }
      case 'N':
      {
       symbol='s';
       break;
      }
      case 's':
      {
       symbol='E';
       break;
      }
      case 'n':
      {
       symbol='W';
       break;
      }
      case 'W':
      {
       symbol='S';
       break;
      }
      case 'S':
      {
       symbol='n';
       break;
      }
     }
     break;
    }
    case 3:
    {
    switch(symbol)
     {
      case 'E':
      {
       symbol='W';
       break;
      }
      case 'W':
      {
       symbol='E';
       break;
      }
      case 'n':
      {
       symbol='s';
       break;
      }
      case 's':
      {
       symbol='n';
       break;
      }
      case 'N':
      {
       symbol='S';
       break;
      }
      case 'S':
      {
       symbol='N';
       break;
      }
     }
     break;
    }
    case 4:
    {
    switch(symbol)
     {
      case 'E':
      {
       symbol='s';
       break;
      }
      case 'n':
      {
       symbol='S';
       break;
      }
      case 'N':
      {
       symbol='E';
       break;
      }
      case 'W':
      {
       symbol='n';
       break;
      }
      case 's':
      {
       symbol='N';
       break;
      }
      case 'S':
      {
       symbol='W';
       break;
      }
     }
     break;
    }
    case 5:
    {
    switch(symbol)
     {
      case 'E':
      {
       symbol='S';
       break;
      }
      case 'n':
      {
       symbol='E';
       break;
      }
      case 'N':
      {
       symbol='n';
       break;
      }
      case 'W':
      {
       symbol='N';
       break;
      }
      case 's':
      {
       symbol='W';
       break;
      }
      case 'S':
      {
       symbol='s';
       break;
      }
     }
     break;
    }
   }
  // System.out.println("-->"+newStructure+"("+newStructure.length()+")");
   newStructure=newStructure+String.valueOf(symbol);
  }

  if(newStructure.length()<aStructure.length())
  {
   newStructure= newStructure+aStructure.substring(where+rLength);
  }
 // System.out.println("oldStructure="+aStructure+"post:"+where+", length:"+rLength+", newStructure="+newStructure);
  return (newStructure);
 }




 public String getReflectSubstructure(String aStructure, int where, int rLength, int reflectType)
 {
  int i;
  String newStructure;
  char symbol;

  newStructure = aStructure.substring(0,where);

  for(i=where;i<(where+rLength);i++)
  {
   symbol=aStructure.charAt(i);
   switch(reflectType)
   {
    /*   {'E','W','N','n','S','s'} */
    case 1:/*reflect rule 1 NS*/
    {
     switch(symbol)
     {
      case 'E':
      {
       symbol='W';
       break;
      }
      case 'n':
      {
       symbol='s';
       break;
      }
      case 'W':
      {
       symbol='E';
       break;
      }
      case 's':
      {
       symbol='n';
       break;
      }

     }
     break;
    }
    case 2:
    {/* reflect rule 2 ns */
    switch(symbol)
     {
      case 'E':
      {
       symbol='W';
       break;
      }
      case 'N':
      {
       symbol='S';
       break;
      }

      case 'W':
      {
       symbol='E';
       break;
      }
      case 'S':
      {
       symbol='N';
       break;
      }
     }
     break;
    }
    case 3:
    {/* this is reflect 3 WE*/
    switch(symbol)
     {

      case 'n':
      {
       symbol='s';
       break;
      }
      case 's':
      {
       symbol='n';
       break;
      }
      case 'N':
      {
       symbol='S';
       break;
      }
      case 'S':
      {
       symbol='N';
       break;
      }
     }
     break;
    }
   }


  // System.out.println("-->"+newStructure+"("+newStructure.length()+")");
   newStructure=newStructure+String.valueOf(symbol);
  }

  if(newStructure.length()<aStructure.length())
  {
   newStructure= newStructure+aStructure.substring(where+rLength);
  }
 // System.out.println("oldStructure="+aStructure+"post:"+where+", length:"+rLength+", newStructure="+newStructure);
  return (newStructure);
 }



 public String getShiftSubstructure(String aStructure, int where, int length)
    {/* UNIMPLEMENTED */
     return null;}

}
