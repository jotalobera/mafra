
package MAFRA.Factory;

import MAFRA.Population_Individual.*;


/** This factory is used to initialize a population with Individuals whose
Chromosome is a BitSet.
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class IndividualBitSetFactory extends IndividualFactory{

  protected ProblemFactory myProblem;

  public IndividualBitSetFactory(ProblemFactory myP)
    {
      myProblem = myP;
    }

  public Individual createNewIndividual()
    {
      BitSetIndividual aI;

      aI = new BitSetIndividual(myProblem);
      return aI;
    }
}
      
 
