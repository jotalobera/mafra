
package MAFRA.Factory;

import MAFRA.Population_Individual.*;

/** This class provides the basic mechanisms by which a problem is defined 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor */

public abstract class ProblemFactory extends Factory{

  protected double size;
  private   String problemName;
  private   String problemGoal;
  protected double numberOfFitnessCalls;

  public ProblemFactory(double aSize,String aName, String aGoal)
    {
      size = aSize;
      problemName = new String(aName);
      numberOfFitnessCalls = 0.0 ;
      problemGoal = new String(aGoal);
    }
 
  public ProblemFactory(double aSize,String aName)
    {
      size = aSize;
      problemName = new String(aName);
      numberOfFitnessCalls = 0.0 ;
    }

  public ProblemFactory(double aSize)
    {
      size = aSize;
      numberOfFitnessCalls = 0.0 ;
    }

  /** This class method is intended to ask the factory to read an Instance from a file.
      Should be redefined by the concrete factory and will return an Object instance that
      contains the newly created problem instance.*/
  public  static Object readInstance(String fileName)
    {
      return null;
    }

  public abstract double fitness(Individual anIndividual);
  
  public abstract boolean feasibilityCheck(Individual anIndividual);

  public abstract Object newSolution();

    //  public abstract char []getAlphabet();
 
    //  public abstract int getAlphabetSize();

  public boolean checkIfvalidInstance()
    {
      return true;
    }

  public void setSize(double aSize)
    {
      size = aSize;
    }

  public double getSize()
    {
      return size;
    }

  public void setName(String aName)
    {
      problemName = new String(aName);
    }

  public String getName()
    {
      if (problemName!= null)
	{
	  return problemName;
	}
      else
	{
	  return ("Undefined Problem Name");
	}
    }

  public double getNumberOfFitnessCalls()
    {
      return numberOfFitnessCalls;
    }

  public String getGoal()
    {
      return problemGoal;
    }

  public void setGoal(String aGoal)
  {
    problemGoal = new String(aGoal);
  }
}


