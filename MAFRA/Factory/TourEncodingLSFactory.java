

package MAFRA.Factory;

import MAFRA.LocalSearcher.*;
/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourEncodingLSFactory extends LocalSearchFactory{

 private boolean edgeEncoding;

 public TourEncodingLSFactory()
 {
  super();
  edgeEncoding = false;
 }

 public TourEncodingLSFactory(boolean aB)
 {
  super();
  edgeEncoding = aB;
 }



 public AbstractLocalSearcher createGeneralHillClimberLocalSearcher(boolean sinOpt, int blindFirstBest, int numIt, int move, ProblemFactory aProblem)
 {
  if(!edgeEncoding)
  {
   TourGeneralHillClimberLocalSearcher aGHC;
   aGHC = new TourGeneralHillClimberLocalSearcher(sinOpt,false, blindFirstBest,numIt,move,aProblem);
   return (aGHC);
   
  }
  else
  {
   return null;
  }
   
 }
 
 public AbstractLocalSearcher createGeneralHillClimberLocalSearcher(boolean sinOpt, boolean hcBhc,int blindFirstBest, int numIt, int move, ProblemFactory aProblem)
 {
  if(!edgeEncoding)
  {
   TourGeneralHillClimberLocalSearcher aGHC;
   aGHC = new TourGeneralHillClimberLocalSearcher(sinOpt,hcBhc, blindFirstBest,numIt,move,aProblem);
   return (aGHC);
   
  }
  else
  {
   return null;
  }
   
 }
 
 
 
 public  AbstractLocalSearcher createBoltzmannLocalSearcher()
   {
    if(!edgeEncoding)
    {
     TourBoltzmannLocalSearcher ls;
     ls = new TourBoltzmannLocalSearcher();
     return (ls);
    }
    else
    {
     TourBoltzmannLocalSearcherEdgeEncoding ls;
     ls = new TourBoltzmannLocalSearcherEdgeEncoding();
     return (ls);
    }
   };



 public  AbstractLocalSearcher createAnnealingLocalSearcher()
   {
    if(!edgeEncoding)
    {
     TourAnnealingLocalSearcher ls;
     ls = new TourAnnealingLocalSearcher();
     return (ls);
    }
    else
    {
     TourAnnealingLocalSearcherEdgeEncoding ls;
     ls = new TourAnnealingLocalSearcherEdgeEncoding();
     return (ls);
    }
   };




}
