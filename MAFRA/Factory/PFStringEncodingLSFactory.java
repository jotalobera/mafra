

package MAFRA.Factory;

import MAFRA.LocalSearcher.*;
/** @version Memetic Algorithms Framework - V 1.2 - December 2000
    @author  Natalio Krasnogor
    */
public class PFStringEncodingLSFactory extends LocalSearchFactory{

 

 public PFStringEncodingLSFactory()
 {
  super();
 
 }

/*public AbstractLocalSearcher createGeneralHillClimberLocalSearcher(boolean sinOpt, int blindFirstBest, int numIt, int move, ProblemFactory aProblem)
 {
 
  return null;
   
 }*/
 
 
 
 public AbstractLocalSearcher createGeneralHillClimberLocalSearcher(boolean sinOpt, boolean hcBhc,int blindFirstBest, int numIt, String move, ProblemFactory aProblem)
 {

   PFGeneralHillClimberLocalSearcher aGHC;
   aGHC = new PFGeneralHillClimberLocalSearcher(sinOpt,hcBhc, blindFirstBest,numIt,move,aProblem);
   return (aGHC);

   
  }
  
  
  
 public AbstractLocalSearcher createGeneralHillClimberLocalSearcherRel(boolean sinOpt, boolean hcBhc,int blindFirstBest, int numIt, String move, PFProblemFactory aProblem)
 {

   PFGeneralHillClimberLocalSearcherRel aGHC;
   aGHC = new PFGeneralHillClimberLocalSearcherRel(sinOpt,hcBhc, blindFirstBest,numIt,move,aProblem);
   return (aGHC);

   
  }
   
 
 
 
 
 public  AbstractLocalSearcher createBoltzmannLocalSearcher()
   {
    return null;
   };



 public  AbstractLocalSearcher createAnnealingLocalSearcher()
   {
    return null;
   };




}



