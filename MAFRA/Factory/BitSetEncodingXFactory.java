
package MAFRA.Factory;

import MAFRA.CrossOver.*;
/** @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */

public class BitSetEncodingXFactory extends CrossOverFactory{


 public  AbstractCrossOver createOnePointCrossOver()
   {
    BitSetOnePointCrossOver oPX;
    oPX = new BitSetOnePointCrossOver();
    return (oPX);
   };

   public  AbstractCrossOver createTwoPointCrossOver()
   {
    BitSetTwoPointCrossOver tPX;
    tPX = new BitSetTwoPointCrossOver();
    return(tPX);
   };

 public  AbstractCrossOver createThreePointCrossOver()
   {
    BitSetThreePointCrossOver thPX;
    thPX = new BitSetThreePointCrossOver();
    return(thPX);
   };

 public  AbstractCrossOver createFourPointCrossOver()
   {
    BitSetFourPointCrossOver fPX;
    fPX = new BitSetFourPointCrossOver();
    return(fPX);
   };


   public AbstractCrossOver createUniformCrossOver()
     {
       BitSetUniformCrossOver uX;

       uX = new BitSetUniformCrossOver();
       return (uX);
     }


   public AbstractCrossOver createBehaviourCrossOver()
     {
       BitSetBehaviourUX uX;

       uX = new BitSetBehaviourUX();
       return (uX);
     }
   


}
