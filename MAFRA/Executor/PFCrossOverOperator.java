


package MAFRA.Executor;

import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.CrossOver.*;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;

/** This class is the one that _Actually_ implements the crossover operator.
It implements the Operator interface which in turns extends executor interface.
This class is USER DEFINED.

@version Memetic Algorithms Framework - V 1.0 - August 1999
@author  Natalio Krasnogor
*/

public class PFCrossOverOperator implements Operator{

  private AbstractCrossOver myCrossOver;
  private FitnessVisitor    myFitnessVisitor;
  private Individual        myParent1;
  private Individual        myParent2;
  private Individual        myOffspring;
  private ProblemFactory    myProblemFactory;

  /** Creates a CrossOver Operator using a specific CrossOver Factory */
 public PFCrossOverOperator(CrossOverFactory aFactory,ProblemFactory myProblem)
   {
    myProblemFactory = myProblem;
    myCrossOver   = ((StringEncodingXFactory)aFactory).createTwoPointCrossOver();
    myFitnessVisitor = new FitnessVisitor(myProblem);
    myParent1 = null;
    myParent2 = null;

   }


 /** There are two individual passed to the CrossOverOperator via the hashtable
     by reference. One of them has an associated key="parent1" and the other key="parent2" */
 public void setArguments(Hashtable args)
   {
    myParent1 = ((Individual)(args.get("parent1"))).copyIndividual();
    myParent2 = ((Individual)(args.get("parent2"))).copyIndividual();
   }


 /** This method returns the newly created offspring in the hashtable
stored with the key "offspring".
 */
 public Hashtable getResults()
   {
     Hashtable aHt;

     aHt = new Hashtable();
     aHt.put("offspring1",myOffspring);
     return aHt;

   }


 /** performs the crossover between parent1 and parent2 which were setted by setArguments() */
 public void execute()
   {
     StringIndividual aNewIndividual;
     if ((myParent1!= null) && (myParent2!= null))
       {
      	 aNewIndividual =(StringIndividual)( ((StringTwoPointCrossOver)myCrossOver).twoParentsCrossover(myParent1,myParent2));
         myOffspring= new SelfAdaptingStaticPFStringIndividual(myProblemFactory);
         myOffspring.setChromosome(aNewIndividual.getChromosome());
      	 myFitnessVisitor.visitIndividual(myOffspring);

      	 //	 System.out.println("################ NewBorns #################");
      	 //	 myOffspring.acceptVisitor(new DisplayVisitor());
       }
     else
       {
       	 System.out.println("ERROR - Parents both null");
       }
   }


}
