



package MAFRA.Executor;

import java.util.Hashtable;
import MAFRA.Population_Individual.*;


/** This is the superclass of all the different methods of mating selection(construction
of the mating pool). 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class SelectionMethod implements Operator{


  protected Population parentsPopulation;
  protected long       matingPoolSize;
  protected Population matingPool;

  public void setArguments(Hashtable aHt)
    {
      parentsPopulation = (Population)(aHt.get("parentsPopulation"));
      matingPoolSize    =  ((Long)aHt.get("matingPoolSize")).longValue();
    }

  public Hashtable getResults()
    {
      Hashtable aHt;
      aHt = new Hashtable();

      aHt.put("matingPool",matingPool);
      return aHt;
    }

  /** redefined by subclasses.*/
  public void execute()
    {}
}
