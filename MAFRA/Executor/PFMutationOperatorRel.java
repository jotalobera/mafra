



package MAFRA.Executor;

import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Mutation.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;


/** This class is the one that _Actually_ implements the mutation operator.
It implements the Operator interface which in turns extends executor interface.
This class is USER DEFINED.

@version Memetic Algorithms Framework - V 1.0 - December 2000
@author  Natalio Krasnogor
*/
public class PFMutationOperatorRel implements Operator{

  private AbstractMutation   myMutation;
  private Individual         myIndividual;

  /** Creates a Mutation Operator using a specifi Mutation Factory */
 public PFMutationOperatorRel(MutationFactory aFactory, ProblemFactory aP)
   {
       //    ((StringEncodingMFactory)aFactory).setAlphabetSize(aP.TRI2DREALPHABETSIZE);
       //    ((StringEncodingMFactory)aFactory).setSimbols(aP.TRI2DREALPHABET);
        ((StringEncodingMFactory)aFactory).setAlphabetSize(((StringData)aP).getAlphabetSize());
        ((StringEncodingMFactory)aFactory).setSimbols(((StringData)aP).getAlphabet());
    
    myMutation   =((StringEncodingMFactory)aFactory).createOnePointMutation();
    myIndividual = null;
   }


 /** The individual passed to the MutationOperator via the hashtable
     is passed by reference.*/
 public void setArguments(Hashtable args)
   {
    myIndividual = ((Individual)args.get("Individual"));
   } 

 
 /** This method returns null because the arguments are passed by reference 
     and nothing new is created. */
 public Hashtable getResults()
   {
     return null;
   }



 public void execute()
   {
     if (myIndividual!= null)
       {
	
	 myIndividual =((StringOnePointMutation)myMutation).individualMutation(myIndividual);
       }
   }
    

}
