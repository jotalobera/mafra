



package MAFRA.Executor;

import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Mutation.*;
import MAFRA.Factory.*;


/** This class is the one that _Actually_ implements the mutation operator for a
self adapting static mutation strategy. Before calling execute both and individual and
a mutation operator should be passed to it.
It implements the Operator interface which in turns extends executor interface.
This class is USER DEFINED.

@version Memetic Algorithms Framework - V 1.0 - August 1999
@author  Natalio Krasnogor
*/
public class SelfAdaptingStaticMutationOperator implements Operator{

  private AbstractMutation myMutation;
  private Individual         myIndividual;

  /** Creates a Mutation Operator using a specifi Mutation Factory */
 public SelfAdaptingStaticMutationOperator()
   {
    myMutation   = null;
    myIndividual = null;
   }


 /** The individual passed to the MutationOperator via the hashtable
     is passed by reference.*/
 public void setArguments(Hashtable args)
   {
    myIndividual = (Individual)(args.get("Individual"));
    myMutation   =((AbstractMutation)args.get("MutationOperator"));
   }

 
 /** This method returns null because the arguments are passed by reference 
     and nothing new is created. */
 public Hashtable getResults()
   {
     return null;
   }



 public void execute()
   {
     if (myIndividual!= null)
       {
	 myIndividual =(Individual)( myMutation.individualMutation((Individual)myIndividual));
       }
   }
    

}
