package MAFRA.Executor;

import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Mutation.*;
import MAFRA.Factory.*;


/** This class is the one that _Actually_ implements the mutation operator.
It implements the Operator interface which in turns extends executor interface.
This class is USER DEFINED.

@version Memetic Algorithms Framework - V 1.0 - August 1999
@author  Natalio Krasnogor
*/
public class MutationOperator implements Operator{

  private AbstractMutation myMutation;
  private Individual         myIndividual;

  /** Creates a Mutation Operator using a specifi Mutation Factory */
 public MutationOperator(MutationFactory aFactory)
   {
  // myMutation   =((TourEncodingMFactory)aFactory).createOnePointMutationEdgeEncoding();
   myMutation   =((BitSetEncodingMFactory)aFactory).createOnePointMutation();
    myIndividual = null;
   }


 /** The individual passed to the MutationOperator via the hashtable
     is passed by reference.*/
 public void setArguments(Hashtable args)
   {
    myIndividual = ((Individual)args.get("Individual"));
   }


 /** This method returns null because the arguments are passed by reference
     and nothing new is created. */
 public Hashtable getResults()
   {
     return null;
   }



 public void execute()
   {
     if (myIndividual!= null)
       {
	// myIndividual =((EdgeEncodingOnePointMutation) myMutation).individualMutation(myIndividual);
	 myIndividual =((BitSetOnePointMutation) myMutation).individualMutation(myIndividual);
       }
   }


}
