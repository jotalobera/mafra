

package MAFRA.Executor;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** This class constructs a mating pool using the parents population by means of
the Tournament selection method.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class TournamentSelectionMethod extends SelectionMethod{

 private   int tournamentSize;
 
 public TournamentSelectionMethod()
 {
  tournamentSize=2;
 }

 public void setSize(int aSize)
 {
  tournamentSize = aSize;
 }
 
 public void execute()
   {
    
     Individual RAMParents[];
     Individual RAMTournament[];
     int i;
     int parentsPopSize;
   
     int pos;
     int maxPos;
     int bigger;
     int j;
     int k;

     matingPool = new Population();
     parentsPopSize = (int)(parentsPopulation.getMembersNumber());
     RAMParents = new Individual[parentsPopSize];
     i = 0;
     for(parentsPopulation.first();parentsPopulation.isInList();parentsPopulation.advance())
       {
	 RAMParents[i] = (Individual)(parentsPopulation.retrieve());
	 i++;
       }
     //System.out.println("TournamentSelectionMethod uses a matingPoolSize="+matingPoolSize);
     //System.out.println("TournamentSelectionMethod uses a parentsPopSize="+parentsPopSize);
 
     RAMTournament = new Individual[tournamentSize];
     for(k=0;k<matingPoolSize;k++)
       {
      
	 for(i=0;i<tournamentSize;i++)
	 {
	  RAMTournament[i] = (Individual) (RAMParents[(int)MAFRA_Random.nextUniformLong(0,parentsPopSize)]);
	 }
	 maxPos = 0;
	 bigger = 0;
	 for(i=0;i<tournamentSize;i++)
	 {
	  pos = 0;
	  for(j=0;j<tournamentSize;j++)
	  {
	   if((i!=j) && (RAMTournament[i].lessThanEq(RAMTournament[j]))) /* lessThan returns TRUE if the receiver is BIGGER */
	   { 
	    pos++;
	   }
	  }
	  if (pos>maxPos)
	  {
	   bigger = i;
	   maxPos = pos;
	  }
	 }
	 
	 matingPool.addIndividual(RAMTournament[bigger].copyIndividual());
	
	
       }/*mating pool building */
     
   }


}
