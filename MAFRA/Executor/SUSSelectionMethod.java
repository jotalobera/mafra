

package MAFRA.Executor;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** This class constructs a mating pool using the parents population by means of
the Stochastic Universal Sampling (Fitness Proportional) selection method.

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public class SUSSelectionMethod extends SelectionMethod{


 
 public SUSSelectionMethod()
 {
  
 }
 
 public void execute()
   {
    
     Individual RAMParents[];
     double     RAMParentsFitnesses[];
     int i;
     int parentsPopSize;
     double minFitness;
     double totalFitness;
     double u;
     double sum;
     int count;


     matingPool          = new Population();
     parentsPopSize      = (int)(parentsPopulation.getMembersNumber());
     RAMParents          = new Individual[parentsPopSize];
     RAMParentsFitnesses = new double[parentsPopSize];
     minFitness          = parentsPopulation.getMinFitness();
     totalFitness        = 0.0;
     i = 0;
     for(parentsPopulation.first();parentsPopulation.isInList();parentsPopulation.advance())
       {
	 RAMParents[i]          = (Individual)(parentsPopulation.retrieve());
	 RAMParentsFitnesses[i] = RAMParents[i].getFitness()+minFitness;
//	 System.out.println("   f["+i+"]="+ RAMParentsFitnesses[i] );
	 totalFitness+=           RAMParents[i].getFitness()+minFitness;
	 i++;
       }
    
    if( (totalFitness>Double.NEGATIVE_INFINITY) && (totalFitness<Double.POSITIVE_INFINITY) )
    {
     u   = MAFRA_Random.nextUniformDouble(0.0,(double)(1.0/matingPoolSize));
     sum = 0.0;
   //  System.out.println("totalFitness="+totalFitness+", parentsPopSize="+parentsPopSize);
     for(i=0;i<parentsPopSize;i++)
     {
      count = 0;
      sum = sum+ ((double)  (RAMParentsFitnesses[i]/totalFitness));
      while(u<sum)
      {
       count++;
       matingPool.addIndividual(RAMParents[i].copyIndividual());
       u = u + ((double)(1.0/matingPoolSize));
      }
    //  System.out.println("   parent["+i+"]="+count);
     }
    }
    else
    {/* CAREFULL!! one or more of the individuals got an infinity value in its fitness */
   //  System.out.println("ERROR - individuals with infinity fitness values, randomly coping to mating pool");
     for(i=0;i<matingPoolSize;i++)
     {
       matingPool.addIndividual(RAMParents[(int)(MAFRA_Random.nextUniformLong(0,parentsPopSize))].copyIndividual());
     }
    }
	 
	 
	
	
   }/*mating pool building */
     
 }



