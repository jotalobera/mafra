



package MAFRA.Executor;


import java.util.Hashtable;
import MAFRA.Executor.Executor;

/** Operator is an interface used to specify a generic operator for the whole system.
Because classes that implements the Operator interface also implements Executor interface
they can be used by Strategy or StrategyExecutor classes.
@version Memetic Algorithms Framework - V 1.0 - July 1999
@author  Natalio Krasnogor
*/

public interface Operator extends Executor{




 /** This method is used to pass arguments to a class that implements a specific operator.
     The arguments are a Hashtable in order to allow
     multiple kind of arguments to operate with. Those arguments will have 
     an associated name which is significant to the concrete class that
      _actually_ implements the operation 
 */
 public abstract void setArguments(Hashtable arguments); 
 
 /* This method permits to access the argument name passed to the object that 
    implement this  interface.
 public abstract void getArgument(String name);*/


 /** This method retrieves results (again implemented as a Hashtable) once the operator was applied.
 */
 public abstract Hashtable getResults();
 
 

}
