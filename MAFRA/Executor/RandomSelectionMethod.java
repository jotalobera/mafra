

package MAFRA.Executor;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** This class constructs a mating pool using the parents population by means of
the Tournament selection method.

 @version Memetic Algorithms Framework - V 1.0 - September 2000
 @author  Natalio Krasnogor
 */
public class RandomSelectionMethod extends SelectionMethod{


 
 
 public void execute()
   {
    
     Individual RAMParents[];
   
     int i;
     int parentsPopSize;
   
     int pos;
     int maxPos;
     int bigger;
     int j;
     int k;

     matingPool = new Population();
     parentsPopSize = (int)(parentsPopulation.getMembersNumber());
     RAMParents = new Individual[parentsPopSize];
     i = 0;
     for(parentsPopulation.first();parentsPopulation.isInList();parentsPopulation.advance())
       {
	matingPool.addIndividual( (Individual)(parentsPopulation.retrieve()).copyIndividual());
	
       }
   
     
   }


}
