

package MAFRA.AbstractPlan;


/** This class implements a simple GA with ONLY  mutation and selection stages (hence the name "Asexual"). 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class AsexualSimpleGAPlan extends SimpleGAPlan{

  public AsexualSimpleGAPlan(GA aGA)
    {
      super(aGA);
    }

  /** In this class we overwrite the DoReproduce from SimpleGAPlan in such a way to do nothing.*/
  public void DoReproduce()
    {}


}
		      
