package MAFRA.AbstractPlan;

import java.util.*;

import  MAFRA.Population_Individual.*;
import  MAFRA.Visitor.*;
import  MAFRA.Util.*;

/** Plan class, which is abstract, implements a Template Method Pattern.
In essence it is the responsible of the interaction between the parts of the
evolutionary system. Its subclasses redefine all or some of the DoXXXX methods
as a way of defining an evolutive plan

 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */
public abstract class Plan extends AbstractPlan{

 protected GA myGA;
 /** This variable hodls the number of past generations.*/
 protected long generationsNow;
 protected Population   previousGenerationPopulation;
 protected boolean      keepPrevious;
 protected Hashtable    logsHashTable;
 protected Population   elitePopulation;
 protected long         elitePopulationSize;
 protected boolean      keepElite;


 public Plan(GA aGA)
   {
     setGenerationsNow(0);
     setGA(aGA);
     keepPrevious = false;
     previousGenerationPopulation = new Population();
     elitePopulation = new Population();
     elitePopulationSize = 0;
     keepElite = false;
     logsHashTable = new Hashtable();
   }

 /** This method sets the size of the elite set to keep */
 public void setElitePopulationSize(long anEliteSize)
 {
  elitePopulationSize=anEliteSize;
 }
 
 /** This method returns the size of the elite set that is being kept */
 public long getElitePopulationSize()
 {
  return elitePopulationSize;
 }
 
 /** This method instructs the GA to keep an elite set */
 public void setElitism()
 {
  keepElite = true;
 }
 
 
 /** This method intructs the GA not to keep an elite set */
 public void resetElitism()
 {
  keepElite = false;
 }
 
 /* This method returns the true if the GA is keeping an elite set or false otherwise */
 public boolean getElitStrategy()
 {
  return keepElite;
 }
 
 /** This method adds aLog to the Plan with the given String as a name. */
 public void setLog(String aName,Log aLog)
   {
     logsHashTable.put(aName,aLog);
   }

 public Log getLog(String aName)
   {
     return (Log)(logsHashTable.get(aName));
   }

 public void setGenerationsNow(long aG)
   {
     generationsNow = aG;
   }

 public long getGenerationsNow()
   {
     return generationsNow;
   }

 public void setGA(GA aGA)
   {
     myGA = aGA;
   }
 
 public GA getGA()
   {
     return myGA;
   }

 /** If keepPrevious was invoked, this function will return
     a copy of the population that the GA contained by the
     Plan used in the previous generation. */
 public Population getPreviousGenerationPopulation()
   {
     return previousGenerationPopulation;
   }


 /** This function is used to enforce the plan to keep a copy of the 
     previous population in each generation. */
 public void keepPrevious()
   {
     keepPrevious = true;
   }

 /** This functions overrides the setting by keepPrevious(), and the plan doesn't
     keep copy of the previous population. */
 public void dontKeepPrevious()
   {
     keepPrevious = false;
   }

 public void DoReproduce()
   {};

 public void DoMutate()
   {};

 public void DoLocalSearch()
   {};

 public void DoUpdatePopulation()
   {};

 public void DoFineGrainSchedulerM()
   {};

 public void DoFineGrainSchedulerX()
   {};

 public void DoCoarseGrainScheduler()
   {};

 public void DoMethaScheduler()
   {};

 public void DoRestartPopulation()
   {}; 

 public void DoInitPopulation()
   {};

 public void DoFinalizePopulation()
   {};

 /** By default this method will iterate through all the log objects and will execute them.*/
 public void DoLog()
   {

     for (Enumeration logs = logsHashTable.elements() ; logs.hasMoreElements() ;)
       {
	 ((Log)(logs.nextElement())).execute();
       }

   };

 /** By default this method will iterate through all the log objects and will end them.*/
 public void DoEndLog()
   {
     for (Enumeration logs = logsHashTable.elements() ; logs.hasMoreElements() ;)
       {
	 ((Log)(logs.nextElement())).endLog();
       }
   }

 public boolean DoCheckTermination()
   {
     return false;
   };

 /** This methods stores in an elite set the best elitNumber elements from the parenstPopulation
     which is supposed to be sorted with the best elements first.*/
 public void DoPickElites()
 {
  int i;
  Population tmpPop;
  
  if(keepElite)
  {
   tmpPop = (myGA.getPopulation("parentsPopulation"));  
   elitePopulation.extinct();
   tmpPop.first();
   for(i=0;i<elitePopulationSize;i++)
   {
    elitePopulation.addIndividual(( tmpPop.retrieve()).copyIndividual());
    tmpPop.advance();
   
   }
  }
 }
 
 
 public void DoPutElites()
 {
  Population tmpPop;
  int i;
  Individual tmpIndi;
   
  if(keepElite&&(generationsNow>0))
  {
   tmpPop = (myGA.getPopulation("offspringsPopulation"));  
   elitePopulation.first();
   tmpPop.first();
   for(i=0;i<elitePopulationSize;i++)
   {
    tmpIndi =(elitePopulation.retrieve()).copyIndividual();
    System.out.println("f(E["+i+"])="+tmpIndi.getFitness());
    tmpPop.addIndividualIfNotInYet(tmpIndi);
    elitePopulation.advance();
   
   }
  } 
 
 }
 
 
   /** Executes the plan.*/
 public void run()
   {
    DoInitPopulation();
    while (!DoCheckTermination())
      {

	if (keepPrevious)
	  {
	    previousGenerationPopulation.extinct();
	    (myGA.getPopulation("parentsPopulation")).copyTo(previousGenerationPopulation);
	  }
	DoLog();
	
	DoReproduce();
	DoMutate();
	DoLocalSearch();  
	DoPutElites();
       	DoUpdatePopulation();
	DoPickElites();
	showPopulation(); 	
	DoRestartPopulation();
	
	generationsNow++;

      }
    DoFinalizePopulation();
    DoEndLog();
   }

 private void showPopulation()
   {
     Population tmpPop;
     Visitor    tmpVis;

     tmpPop = myGA.getPopulation("parentsPopulation");
     tmpVis = myGA.getVisitor("displayVisitor");
     tmpPop.acceptVisitor(tmpVis);
     
   }

}
