
package MAFRA.AbstractPlan;


/** This class implements a simple MA with ONLY  mutation, local search and selection stages (hence the name "Asexual"). 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class AsexualSimpleMemeticPlan extends SimpleMemeticPlan{

  public AsexualSimpleMemeticPlan(GA aGA)
    {
      super(aGA);
    }

  /** In this class we overwrite the DoReproduce from SimpleMemeticPlan in such a way to do nothing.*/
  public void DoReproduce()
    {}


}
		      
