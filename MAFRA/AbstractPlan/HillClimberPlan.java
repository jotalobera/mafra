
package MAFRA.AbstractPlan;




import MAFRA.Strategy.*;
import MAFRA.Visitor.*;
import MAFRA.Population_Individual.*;


/** This class implements a simple MA with ONLY local search stage 
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */

public class HillClimberPlan extends SimpleMemeticPlan{


  public HillClimberPlan(GA aGa)
    {
      super(aGa);
    }



    /** Overrides DoReprodoce in SimpleMemeticPlan to perform nothing */
  public void DoReproduce()
  {}
    /** Overrides DoMutate in SimpleMemeticPlan to perform nothing */
  public void DoMutate()
  {}

  


  public void DoUpdatePopulation()
    {
      Population myP;
      Population myOf;
      FitnessVisitor aFS;
      SortingVisitor aSV;
        
	System.out.println("Updating Stage");
      	myP  = myGA.getPopulation("parentsPopulation");
	aFS = (FitnessVisitor)(myGA.getVisitor("fitnessVisitor"));
	myP.acceptVisitor(aFS);
	myP.acceptVisitor(new SortingVisitor());
	System.out.println("Best Parent"+myP.getMaxFitness()+" Worst Parent:"+myP.getMinFitness());
 	


    }




}
		      
