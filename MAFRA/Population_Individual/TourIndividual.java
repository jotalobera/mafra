
package MAFRA.Population_Individual;


import java.awt.*;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;


public class TourIndividual extends Individual {

  private int numberGenerationsWithoutChange;
  private double temperature;

 public TourIndividual()
   {
     numberGenerationsWithoutChange=0;
     temperature = 0.0;
   }

 public TourIndividual(ProblemFactory aPF)
   {
     super(aPF);
     temperature = 0.0;
     numberGenerationsWithoutChange=0;
   }



 public boolean equals(Individual anIndividual)
   {
     int i;
     int size;
     boolean allEqual;
     int    [] chrom1;
     int     [] chrom2;


     /* size     =(int)( anIndividual.getSize());
     allEqual = true;
     chrom1   = (int [])(anIndividual.getChromosome());
     chrom2   = (int [])(this.getChromosome());



     for (i=0;i<size;i++)
       {
	 allEqual = ((chrom1[i]==chrom2[i]) && allEqual);
       }

       */
     allEqual = (anIndividual.getFitness() == this.getFitness());

     return (allEqual);
   }





 public void setTemperature(double aTemp)
   {
     temperature = aTemp;
   }

 public double  getTemperature()
   {
     return temperature;
   }

 public void setChromosome(Object aChr)
   {
     int i;

     chromosome = new int[(int)size];
     for (i=0;i<size;i++)
       {
	        ((int [])chromosome)[i] = ((int [])aChr)[i] ;
       }
   }

 public int getNumberGenerationsWithoutChange()
   {
     return numberGenerationsWithoutChange;
   }

 public void setNumberGenerationsWithoutChange(int anInt)
   {
     numberGenerationsWithoutChange = anInt;
   }

 public Individual copyIndividual()
   {
     TourIndividual aNewOne;


     aNewOne = new TourIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());
     aNewOne.setChromosome(this.getChromosome());
     aNewOne.setNumberGenerationsWithoutChange(this.getNumberGenerationsWithoutChange());
     aNewOne.setTemperature(this.getTemperature());
     return (Individual)aNewOne;

   }


 public String chromosomeToString()
 {

   StringBuffer tmp;
   int i;
   int aTour[];

   aTour = (int[])(this.getChromosome());
   tmp = new StringBuffer();
   tmp.append(aTour[0]);
   for (i=1;i<size;i++)
   {
      tmp.append("->" + aTour[i]);
   }
   return tmp+"";
 }

 public String renderIndividualData()
 {

    return("|S="+size+",F="+(-fitness)+"|<"+(this.chromosomeToString())+">");
 }


 public void display()
   {
     System.out.println(this.renderIndividualData());
   }



 public void display(TextArea myTextArea)
   {
     myTextArea.append(this.renderIndividualData()+"\n");
   }


 public void display(Canvas aCanvas, double citiesXY[][], double minX, double maxX, double minY, double maxY)
   {
     int i;
     Color cityColor = Color.blue;
     Color lineColor = Color.red;
     Graphics aGraphic;
     double x1;
     double y1;
     double x2;
     double y2;
     double xScale;
     double yScale;
     double deltaY;
     double deltaX;
     int    myChromosome[];


     myChromosome = (int [])chromosome;
     aGraphic = aCanvas.getGraphics();
     xScale   = (aCanvas.getSize().width-10.0)/(Math.abs(maxX-minX)+0.0);
     yScale   = (aCanvas.getSize().height-10.0)/(Math.abs(maxY-minY)+0.0);

     deltaY   = -minY;
     deltaX   = -minX;

     for (i=0;i<size-1;i++)
       {
         x1 =  xScale*(citiesXY[myChromosome[i]][0]+ deltaX)+5;

	       y1 =  yScale*(citiesXY[myChromosome[i]][1]+deltaY )+5;


      	x2 =  xScale*(citiesXY[myChromosome[i+1]][0] + deltaX )+5;


	      y2 =  yScale*(citiesXY[myChromosome[i+1]][1] + deltaY )+5;

      	plotCity(aGraphic,x1,y1,cityColor);
      	plotCity(aGraphic,x2,y2,cityColor);
      	myDrawLine(aGraphic,x1,y1,x2,y2,lineColor);
       }

  	 x1 = xScale*(citiesXY[myChromosome[(int)size-1]][0]+ deltaX)+5;
  	 y1 = yScale*(citiesXY[myChromosome[(int)size-1]][1]+deltaY)+5;

  	 x2 =xScale*(citiesXY[myChromosome[0]][0]+deltaX)+5;
  	 y2 =yScale*(citiesXY[myChromosome[0]][1]+deltaY)+5;

  	plotCity(aGraphic,x1,y1,cityColor);
  	plotCity(aGraphic,x2,y2,cityColor);
  	myDrawLine(aGraphic,x1,y1,x2,y2,lineColor);


   }

 private void plotCity(Graphics aG,double x,double y,Color aC)
   {
     Color tmp;


     tmp = aG.getColor();
     aG.setColor(aC);
     aG.fillRect((int)(x-2),(int)(y-2),4,4);
     aG.setColor(tmp);
   }

 private void myDrawLine(Graphics aG,double x1, double y1, double x2, double y2, Color aC)
   {
     Color tmp;


     tmp = aG.getColor();
     aG.setColor(aC);
     aG.drawLine((int)x1,(int)y1,(int)x2,(int)y2);
     aG.setColor(tmp);
   }




}
