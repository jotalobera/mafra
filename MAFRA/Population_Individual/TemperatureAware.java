
package MAFRA.Population_Individual;

/** Interface for all the subclasses of Individual that self adapt staticly.

  @version Memetic Algorithms Framework - V 1.0 - February 2020
  @author  Natalio Krasnogor
   */

public interface TemperatureAware{

  public void setTemperature(double aTemp);
  public double  getTemperature();


}
