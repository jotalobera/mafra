
package MAFRA.Population_Individual;


import java.awt.*;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;


public class SelfAdaptingStaticPFStringIndividual extends PFStringIndividual implements SelfAdaptingStatic{

  private int myHelperId;

 public SelfAdaptingStaticPFStringIndividual()
   {
    super();
    myHelperId=-1;
   }

 public SelfAdaptingStaticPFStringIndividual(ProblemFactory aPF)
   {
     super(aPF);
     myHelperId=-1;
   }

 public void setHelperId(int helperId)
   {
     myHelperId = helperId;
   }

  public int getHelperId()
   {
     return myHelperId;
   }


 public void setChromosome(Object aChr, int helperId)
   {
     setChromosome(aChr);
     myHelperId = helperId;
   }



 public Individual copyIndividual()
   {
     SelfAdaptingStaticPFStringIndividual aNewOne;


     aNewOne = new SelfAdaptingStaticPFStringIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());
     aNewOne.setChromosome(this.getChromosome());
     aNewOne.setNumberGenerationsWithoutChange(this.getNumberGenerationsWithoutChange());
     aNewOne.setTemperature(this.getTemperature());
     aNewOne.setPoints(this.getPoints());
     aNewOne.setHelperId(this.getHelperId());
     return (Individual)aNewOne;

   }
   
   


 public void display()
   {
     System.out.println("|S="+size+",F="+fitness+",H="+this.getHelperId()+"|<"+chromosome+">");
   }



 public void display(TextArea myTextArea)
   {
     myTextArea.append("|S="+size+",F="+fitness+",H="+this.getHelperId()+"|<"+chromosome+">\n");
   }

/*
 public void display(Canvas aCanvas, String instance)
 {
  super(aCanvas,instance);
 }*/

}
