

package MAFRA.Population_Individual;

import java.util.BitSet;
import java.awt.TextArea;
import MAFRA.Factory.*;
import MAFRA.Visitor.*;
 

public class SelfAdaptingStaticBitSetIndividual extends BitSetIndividual implements SelfAdaptingStatic {

 private int myHelperId;

 public SelfAdaptingStaticBitSetIndividual(ProblemFactory aPF)
   {
     super(aPF);
     myHelperId=-1;
   }

 public SelfAdaptingStaticBitSetIndividual()
   {
     super();
     myHelperId=-1;
   }

 public void setHelperId(int helperId)
   {
     myHelperId = helperId;
   }

  public int getHelperId()
   {
     return myHelperId;
   }

 public void setChromosome(Object aChr, int helperId)
   {
     setChromosome(aChr);
     myHelperId = helperId;
   }


 public Individual copyIndividual()
   {
     SelfAdaptingStaticBitSetIndividual aNewOne;
     BitSet           aNewC;

     aNewOne = new SelfAdaptingStaticBitSetIndividual();

     aNewOne.setFitness(this.getFitness());
     aNewOne.setSize(this.getSize());

     aNewC = new BitSet();
     aNewC.or((BitSet)(this.getChromosome())) ;

     aNewOne.setChromosome(aNewC,myHelperId);
     return (Individual)aNewOne;

   }



 public void display()
   {
    StringBuffer tmp;
    int i;
    BitSet aBS;
   
    aBS=(BitSet)(this.getChromosome());
    tmp = new StringBuffer();
    for (i=0;i<size;i++)
      {
	if(aBS.get(i))
	  {
	    tmp = tmp.append(1);
	  }
	else
	  {
	    tmp = tmp.append(0);
	  }
      }


     System.out.println("|S="+size+",F="+fitness+"|<"+tmp.toString()+">"+"Helper:"+myHelperId);
   }



 public void display(TextArea myTextArea)
   {
    StringBuffer tmp;
    int i;
    BitSet aBS;
   
    aBS=(BitSet)(this.getChromosome());
    tmp = new StringBuffer();
    for (i=0;i<size;i++)
      {
	if(aBS.get(i))
	  {
	    tmp = tmp.append(1);
	  }
	else
	  {
	    tmp = tmp.append(0);
	  }
      }


     myTextArea.append("|S="+size+",F="+fitness+"|<"+tmp.toString()+">"+"Helper:"+myHelperId+"\n");
   }



}
