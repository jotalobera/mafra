package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;





/** This class is an example of the application to the TSP problem of an evolutionary
approach. 

 @version Memetic Algorithms Framework - V 1.0 - September 1999
 @author  Natalio Krasnogor
 */
public class TSPTest extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SimpleMemeticPlan                             myPlan;

     /* CrossOver Stage Variables */
     MatingStrategy                        myMatingStrategy;
     TourEncodingXFactory                  myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                         myMutationStrategy;
     TourEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     LocalSearchStrategy4Individuals                   myLocalSearchStrategy;
     TourEncodingLSFactory4Individuals                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuPlusLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     TSPProblem                         myProblem;
     IndividualTourFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     TSPAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;



     /* Log Variables */
     GenFitEvsFitTempLog                       aLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;

     
     /* Gral Initialization */
     MAFRA_Random.initialize(117351317);
     aSortingVisitor = new SortingVisitor();



     myProblem       =(TSPProblem)( TSPProblem.readInstance(argv[0]));
     aDisplayVisitor = new TSPAWTDisplayVisitor(200,200,myProblem.getMinX(),myProblem.getMaxX(),myProblem.getMinY(),myProblem.getMaxY(),false,myProblem.getPositions());
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new IndividualTourFactory(myProblem);
     myPopulation        = new Population(50,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,50);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);




     /* Mutation Stage Initalization */
     myMutationFactory   = new TourEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.4));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new TourEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new MatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.8));
     args.put("lambda",new Long(50));
     args.put("matingPoolSize", new Long(50));
     myMatingStrategy.setArguments(args); 

     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new TourEncodingLSFactory4Individuals();              
     myLocalSearchStrategy = new LocalSearchStrategy4Individuals(myLocalSearchFactory);
     args = new Hashtable();
     args.put("Population",myPopulation);
     args.put("ProbabilityPerIndividual", new Double(1.0));
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args); 

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuPlusLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,50,5);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(2000));



     myPlan = new SimpleMemeticPlan(myGa);

     aLog = new GenFitEvsFitTempLog(myPlan,"TSP-ATT532-Log.dat",1,myProblem," #Gen #FitEvs Fit Temp ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();


    


   }
}
