package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;






/** This class is an example of the application to the PF problem of an evolutionary approach. 
This implements a Memetic Algorithm with:
[1] Tournament Selection for constructing the mating pool, size=4.
[2] (Mu,Lambda) selection strategy.
[3] No Local Search.
[4] 2-Point String Crossover, and one-point mutation.
[5] No Heuristic Initialization .
 @version Memetic Algorithms Framework - V 1.2 - december 2000
 @author  Natalio Krasnogor
*/
public class GPF extends Test{

  public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SelfAdaptingStaticSimpleMemeticPlan      myPlan;
     //HillClimberPlan      myPlan;

     /* CrossOver Stage Variables */
     PFMatingStrategy                        myMatingStrategy;
     StringEncodingXFactory                  myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     PFMutationStrategy                         myMutationStrategy;
     StringEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     PFSelfAdaptingStaticLocalSearchStrategy                   myLocalSearchStrategy;
     PFStringEncodingLSFactory                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     PFProblem                         myProblem;
     SelfAdaptingStaticIndividualPFStringFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     PFAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;
     MemeExpressionVisitor                 aMemeExpressionVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog                       aLog;
     MemesEvolutionaryActivityLog                                            memesEvolutionaryActivityLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     long gamma;
     boolean displayAny;
     boolean displayAll;
     AbstractLocalSearcherProbConExpTable       myLocalSearchProbConExpTable;
     int  numberOfHelpers=1;
     
     String move;
     int firstBest;
     int numIt;
     
     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu         = (Integer.parseInt(argv[3]));
     lambda     = (Integer.parseInt(argv[4]));
     gamma      = (Integer.parseInt(argv[5]));
     displayAny = (Boolean.valueOf(argv[6])).booleanValue();
     displayAll = (Boolean.valueOf(argv[7])).booleanValue();
   /*  move       = (Integer.parseInt(argv[7]));*/ /* values: 1=2-exchange ,2=3-exchange ,3=4-exchange */
   /*  firstBest  = (Integer.parseInt(argv[8]));*/ /* values: 0=blind      ,1= first     ,2=best       */
   /*  numIt      = (Integer.parseInt(argv[9]));*/
       move      = "";
       firstBest = 0;
       numIt     = 0;

     myProblem       =(PFProblem)(new  PFProblem(argv[0]));
     aDisplayVisitor = new PFAWTDisplayVisitor(displayAll,displayAny,argv[0]);
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new SelfAdaptingStaticIndividualPFStringFactory(myProblem,numberOfHelpers);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);



     /* Mutation Stage Initalization */
     myMutationFactory   = new StringEncodingMFactory(myProblem);
     myMutationFactory.setAlphabetSize(5);
     myMutationFactory.setSimbols(PFProblem.TRI2DABALPHABET);
     myMutationStrategy  = new PFMutationStrategy(myMutationFactory,myProblem);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(argv[9]));
     /* LEARNING PROBABILITY */
     args.put("ProbabilityPerIndividualLink", new Double(0.0));
     args.put("NumberOfHelpers", new Integer(numberOfHelpers));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new StringEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod(); 
     myMatingSelectionMethod.setSize(2);
     myMatingStrategy         = new PFMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(argv[10]));
     args.put("lambda",new Long(mu));
     args.put("matingPoolSize", new Long(mu));
     args.put("gamma",new Long(gamma)); /* intermediate population */
     myMatingStrategy.setArguments(args); 

    
  


     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new PFStringEncodingLSFactory();              
     /* Meme table contruction stage */
     myLocalSearchProbConExpTable   = new AbstractLocalSearcherProbConExpTable(numberOfHelpers);
     /* myLocalSearchFactory.createGeneralHillClimberLocalSearcher(sinOpt,blindFirstBest,numIt,move)*/
      myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,0,numIt,new String("*-->!1"),myProblem),0.0);	
     myLocalSearchStrategy = new PFSelfAdaptingStaticLocalSearchStrategy( myLocalSearchProbConExpTable);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args); 
     
     

    
     aMemeExpressionVisitor = new MemeExpressionVisitor(myLocalSearchProbConExpTable);





     

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addVisitor("memeExpressionVisitor",aMemeExpressionVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(argv[8]));



    myPlan = new SelfAdaptingStaticSimpleMemeticPlan(myGa);
   //  myPlan = new HillClimberPlan(myGa);

     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-SAS-PFGeneralHillClimber-"+argv[1]+"-LOG.dat",1,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
   
     memesEvolutionaryActivityLog = new MemesEvolutionaryActivityLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-SAS-PFGeneralHillClimber-"+argv[1]+"-MEEA.dat",1," #Gen AvgFit ExVaMeme1 ExVaMeme2 ... ExVaMemeN : EvAcMeme1 EvAcMeme2 ... EvAcMemeN",myLocalSearchProbConExpTable);
     myPlan.setLog("MemesEvolutionaryActivityLog",memesEvolutionaryActivityLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);


   }
}
