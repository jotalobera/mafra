package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;






/** This class is an example of the application to the TSP problem of an evolutionary approach. 
This implements a Memetic Algorithm with:
[1] Random Selection for constructing the mating pool.
[2] (Mu,Lambda) selection strategy.
[3] Multi memes  as local searcersh which are an adaptive general hill climber.
[4] DPX crossover and double bridge mutation.
[5] DPX mating strategy continuos.
 @version Memetic Algorithms Framework - V 1.2 - August 2000
 @author  Natalio Krasnogor
*/
public class MultiMemeTSP extends Test{

  public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SelfAdaptingStaticSimpleMemeticPlan      myPlan;
     //HillClimberPlan      myPlan;

     /* CrossOver Stage Variables */
     DPXMatingStrategy2                        myMatingStrategy;
     TourEncodingXFactory                  myCrossOverFactory;
     RandomSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                         myMutationStrategy;
     TourEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     SelfAdaptingStaticLocalSearchStrategy                   myLocalSearchStrategy;
     TourEncodingLSFactory                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
    MuLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     TSPProblem                         myProblem;
     SelfAdaptingStaticIndividualTourFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     TSPAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;
     MemeExpressionVisitor                 aMemeExpressionVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog                       aLog;
     MemesEvolutionaryActivityLog                                            memesEvolutionaryActivityLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     long gamma;
     boolean displayAny;
     AbstractLocalSearcherProbConExpTable       myLocalSearchProbConExpTable;
     int  numberOfHelpers=10;
     
     int move;
     int firstBest;
     int numIt;
     
     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu         = (Integer.parseInt(argv[3]));
     lambda     = (Integer.parseInt(argv[4]));
     gamma      = (Integer.parseInt(argv[5]));
     displayAny = (Boolean.valueOf(argv[6])).booleanValue(); //M2612-1
     move       = (Integer.parseInt(argv[7])); /* values: 1=2-exchange ,2=3-exchange ,3=4-exchange */
     firstBest  = (Integer.parseInt(argv[8])); /* values: 0=blind      ,1= first     ,2=best       */
     numIt      = (Integer.parseInt(argv[9]));

     myProblem       =(TSPProblem)( TSPProblem.readInstance(argv[0]));
     aDisplayVisitor = new TSPAWTDisplayVisitor(200,200,myProblem.getMinX(),myProblem.getMaxX(),myProblem.getMinY(),myProblem.getMaxY(),false,displayAny,myProblem.getPositions());
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new SelfAdaptingStaticIndividualTourFactory(myProblem,numberOfHelpers);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);



     /* Mutation Stage Initalization */
     myMutationFactory   = new TourEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     /* PM=0.4 */
     args.put("ProbabilityPerIndividual", new Double(0.4));
     /* LEARNING PROBABILITY */
     args.put("ProbabilityPerIndividualLink", new Double(0.125));
     args.put("NumberOfHelpers", new Integer(numberOfHelpers));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new TourEncodingXFactory();
     myMatingSelectionMethod = new RandomSelectionMethod(); 
//     myMatingSelectionMethod.setSize(4);
     myMatingStrategy         = new DPXMatingStrategy2(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     /* PX=0.6 */
     args.put("matingProbability",new Double(0.6));
     args.put("lambda",new Long(mu));
     args.put("matingPoolSize", new Long(mu));
     args.put("gamma",new Long(gamma)); /* intermediate population */
     myMatingStrategy.setArguments(args); 

    
  


     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new TourEncodingLSFactory();              
         /* Meme table contruction stage */
	 myLocalSearchProbConExpTable   = new AbstractLocalSearcherProbConExpTable(numberOfHelpers);
	 /* myLocalSearchFactory.createGeneralHillClimberLocalSearcher(sinOpt,hcBhc,blindFirstBest,numIt,move)*/
	
	 
        myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,1,1,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,3,1,myProblem),1.0);	

        myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,1,2,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,3,2,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,6,2,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,9,2,myProblem),1.0);	
	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,1,3,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,3,3,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,6,3,myProblem),1.0);	
	myLocalSearchProbConExpTable.addMeme(myLocalSearchFactory.createGeneralHillClimberLocalSearcher(false,false,1,9,3,myProblem),1.0);			

       
     myLocalSearchStrategy = new SelfAdaptingStaticLocalSearchStrategy( myLocalSearchProbConExpTable);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args); 
     
     

    
     aMemeExpressionVisitor = new MemeExpressionVisitor(myLocalSearchProbConExpTable);





     

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addVisitor("memeExpressionVisitor",aMemeExpressionVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(510));



    myPlan = new SelfAdaptingStaticSimpleMemeticPlan(myGa);
   //  myPlan = new HillClimberPlan(myGa);

     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-MultiMemes-"+argv[1]+"-LOG.dat",10,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
   
     memesEvolutionaryActivityLog = new MemesEvolutionaryActivityLog(myPlan,argv[0]+"-"+mu+"-"+lambda+"_by_"+gamma+"-MultiMemes-"+argv[1]+"-MEEA.dat",10," #Gen AvgFit ExVaMeme1 ExVaMeme2 ... ExVaMemeN : EvAcMeme1 EvAcMeme2 ... EvAcMemeN",myLocalSearchProbConExpTable);
     myPlan.setLog("MemesEvolutionaryActivityLog",memesEvolutionaryActivityLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);


   }
}
