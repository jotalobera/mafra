package MAFRA.Test;



import java.util.*;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;
import MAFRA.CrossOver.*;
import MAFRA.Executor.*;
import MAFRA.Factory.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Mutation.*;
import MAFRA.Population_Individual.*;
import MAFRA.SelectionStrategy.*;
import MAFRA.Strategy.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;





/** This class is an example of the application to the TSP problem of an evolutionary approach. 
This implements a Memetic Algorithm with:
[1] Tournament Selection for constructing the mating pool.
[2] A local search which is an adaptive boltzmann hill climber whose
    temperature is set accordingly to the spread of fitnesses within 
    the population. The best individual of the population is not localy serched
    (hence we say it is an ElitistAdaptiveBolztmann
[3] Random initial population
[4] DPXcrossover
[5] (Mu,Lambda) selection strategy
[6] PX=0.8, PM=0.05
[7] edge encoding
 @version Memetic Algorithms Framework - V 1.2 - November 1999
 @author  Natalio Krasnogor
*/
public class TSPTestElitistAdaptiveBoltzmannMuLambdaE extends Test{


 public static void  main(String []argv) throws CloneNotSupportedException
   {
     
     // M212-8 I added the mu and lambda to the argv[] vector

     /* General Evolutionary Plan Variables */
     EvolutionaryPlan                         theEvolutionaryPlan;
     GA                                       myGa; 
     SimpleMemeticPlan                             myPlan;

     /* CrossOver Stage Variables */
     DPXMatingStrategy                        myMatingStrategy;
     TourEncodingXFactory                  myCrossOverFactory;
     TournamentSelectionMethod             myMatingSelectionMethod;

     /* Mutation Stage Variables */
     MutationStrategy                         myMutationStrategy;
     TourEncodingMFactory                     myMutationFactory;


     /* LocalSearch Stage Variables */
     ElitistLocalSearchStrategy                   myLocalSearchStrategy;
     TourEncodingLSFactory                 myLocalSearchFactory; 
     

     /* Selection Stage Variables */
     SelectionStrategy                          mySelectionStrategy;
     MuLambdaSelectionStrategyExecutor      myMPLSExecutor;

     /* Problem and Individual Variables */
     TSPProblemEdgeEncoding                        myProblem;
     IndividualTourFactory               myFactory;

     /* Individual and Population Variables */
     Population                            myPopulation;
     Population                            myOffsprings;



     /* Visitors Variables */
     FitnessVisitor                        aFitnessVisitor;
     TSPAWTDisplayVisitor                  aDisplayVisitor;
     SortingVisitor                        aSortingVisitor;



     /* Log Variables */
     GenFitEvsFitTempDivLog                       aLog;

     /* Gral Variables */
     Hashtable                             args;
     long i,j,gene;
     long mu;
     long lambda;
     long gamma; /* size of intermediate population */
     boolean displayAny ; //M2612-1
     
     
     
     
     
     /* Gral Initialization */
     //     MAFRA_Random.initialize(117351317);
     MAFRA_Random.initialize( (Integer.parseInt(argv[2])) );
     aSortingVisitor = new SortingVisitor();
     mu     = (Integer.parseInt(argv[3]));
     lambda = (Integer.parseInt(argv[4]));
     gamma  = (Integer.parseInt(argv[5]));
     displayAny = (Boolean.valueOf(argv[6])).booleanValue(); //M2612-1
     

     myProblem       =(TSPProblemEdgeEncoding)( TSPProblemEdgeEncoding.readInstance(argv[0]));
     aDisplayVisitor = new TSPAWTDisplayVisitor(200,200,myProblem.getMinX(),myProblem.getMaxX(),myProblem.getMinY(),myProblem.getMaxY(),false,displayAny,myProblem.getPositions());
     aFitnessVisitor = new FitnessVisitor(myProblem);


     /* Populations Initialization*/
     myFactory           = new IndividualTourFactory(myProblem);
     myPopulation        = new Population(mu,myFactory);
     myPopulation.setName("Parents");
     myPopulation.acceptVisitor(aDisplayVisitor);
     myOffsprings = new Population();
     myPopulation.copyTo(myOffsprings,lambda);
     myOffsprings.setName("Offsprings");
     myOffsprings.acceptVisitor(aDisplayVisitor);




     /* Mutation Stage Initalization */
     myMutationFactory   = new TourEncodingMFactory();
     myMutationStrategy  = new MutationStrategy(myMutationFactory);
     args = new Hashtable();
     args.put("Population",myOffsprings);
     args.put("ProbabilityPerIndividual", new Double(0.35));
     myMutationStrategy.setArguments(args);




     /* CrossOver Stage Initialization */
     myCrossOverFactory      = new TourEncodingXFactory();
     myMatingSelectionMethod = new TournamentSelectionMethod();
     myMatingStrategy         = new DPXMatingStrategy(myCrossOverFactory,myMatingSelectionMethod,myProblem);
     args = new Hashtable();
     args.put("offspringsPopulation",myOffsprings);
     args.put("parentsPopulation",myPopulation);
     args.put("matingProbability",new Double(0.0));
     args.put("lambda",new Long(lambda));
     args.put("matingPoolSize", new Long(mu));
     args.put("gamma",new Long(gamma)); /* intermediate population */
     myMatingStrategy.setArguments(args); 

     /* Local Search Stage Initialization */
     myLocalSearchFactory  = new TourEncodingLSFactory(true);  
     /* true is used when edge encoding is used */            
     myLocalSearchStrategy = new ElitistLocalSearchStrategy(myLocalSearchFactory);
     args = new Hashtable();
     args.put("Population",myPopulation); // was myOffsprings M2711-3
     args.put("ProbabilityPerIndividual", new Double(0.0));
     args.put("ProblemFactory",myProblem);
     myLocalSearchStrategy.setArguments(args); 

     /* Selection Stage Initialization */
     myMPLSExecutor      = new MuLambdaSelectionStrategyExecutor(myPopulation,myOffsprings,mu,lambda);
     mySelectionStrategy = new SelectionStrategy(myMPLSExecutor);
	
     /* GA setting */
     myGa = new GA();
     myGa.addMatingStrategy(myMatingStrategy); 
     myGa.addMutationStrategy(myMutationStrategy);
     myGa.addSelectionStrategy(mySelectionStrategy); 
     myGa.addLocalSearchStrategy(myLocalSearchStrategy); 
     myGa.addVisitor("sortingVisitor",aSortingVisitor);
     myGa.addVisitor("displayVisitor",aDisplayVisitor);
     myGa.addVisitor("fitnessVisitor",aFitnessVisitor);
     myGa.addPopulation("parentsPopulation",myPopulation);
     myGa.addPopulation("offspringsPopulation",myOffsprings);
     myGa.addParameter("maxGenerationsNumber",new Long(3000));



     myPlan = new SimpleMemeticPlan(myGa);

     aLog = new GenFitEvsFitTempDivLog(myPlan,argv[0]+"-Log-"+mu+"-"+lambda+"by"+gamma+"-Elitist-Tadaptive-Edge"+argv[1]+".dat",1,myProblem," #Gen #FitEvs Fit Temp Div ");
     myPlan.setLog("GenerationsFitnessEvaluationsFitnessLog",aLog);
     theEvolutionaryPlan = new EvolutionaryPlan(myGa,myProblem,myPlan,null,null);
     theEvolutionaryPlan.run();

     aDisplayVisitor.close();
     System.exit(0);


   }
}
