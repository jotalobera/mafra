


package MAFRA.Visitor;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** This Visitor class is responsable for moving through the population and computing the evolutionary
activity and the expression  of each memes as described in "Visualizing Evolutionary Activity of Genotypes" of Bedau et all.

    @version Memetic Algorithms Framework - V 1.2 - February 2000
    @author  Natalio Krasnogor
    */

public class MemeExpressionVisitor extends Visitor {

  protected AbstractMemeProbConExpTable myHelpers;
  protected long evolutionaryActivity[];
  protected int size;

  public  MemeExpressionVisitor(AbstractMemeProbConExpTable helpers)
    {
      myHelpers            = helpers;
      size                 = helpers.getNumberOfHelpers();
      evolutionaryActivity = new long[size];
    }

  //public void visitIndividual(SelfAdaptingStaticTourIndividual anIndividual)
  public void visitIndividual(SelfAdaptingStatic anIndividual)
    {
      int helperId;
      int expression;
      helperId = anIndividual.getHelperId();
      evolutionaryActivity[helperId] = evolutionaryActivity[helperId]+1;
      
    };

  public void visitPopulation(Population aPopulation)
    {
      Population membersOfPop;
   //   SelfAdaptingStaticTourIndividual anIndividual;
      SelfAdaptingStatic anIndividual;
      int i;

    //  System.out.println("Computing the expression of memes");
      for (i=0;i<size;i++)
	{
	  evolutionaryActivity[i] = 0;
	}
      membersOfPop = aPopulation;
      for( membersOfPop.first( ); membersOfPop.isInList( ); membersOfPop.advance( ) )
        {
      //   anIndividual =(SelfAdaptingStaticTourIndividual)(membersOfPop.retrieve());
         anIndividual =(SelfAdaptingStatic)(membersOfPop.retrieve());
         this.visitIndividual(anIndividual);
	}


      for(i=0;i<size;i++)
	{
	  if(evolutionaryActivity[i]!=0)
	    {// this is for the integral
	     myHelpers.setEvolutionaryActivityValue(i,myHelpers.getEvolutionaryActivityValue(i) + (int)evolutionaryActivity[i]);
      	     // this is for the expression of each meme
	      myHelpers.setExpressionValue(i, (int)evolutionaryActivity[i]);
	    }
	  else
	    {
	      myHelpers.setExpressionValue(i, 0);
	      myHelpers.setEvolutionaryActivityValue(i,0.0);
	    }
	}
      

    
    };



}

