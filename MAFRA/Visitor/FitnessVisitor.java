


package MAFRA.Visitor;

import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;

/** This Visitor class is responsable for moving through the population and compute fitness of each individual.
Also provides a means to visit a particular individual. Mantains the data of the max, min, avg fitnesses.
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public class FitnessVisitor extends Visitor {

  protected ProblemFactory theProblem;

  public FitnessVisitor(ProblemFactory aProblem)
    {
      theProblem = aProblem;
    }

  public void visitIndividual(Individual anIndividual)
    {
      anIndividual.setFitness(theProblem.fitness(anIndividual));
    };

  public void visitPopulation(Population aPopulation)
    {
      Population membersOfPop;
      Individual anIndividual;

      membersOfPop = aPopulation;
      membersOfPop.resetFitnesses();
      for( membersOfPop.first( ); membersOfPop.isInList( ); membersOfPop.advance( ) )
        {
         anIndividual =(Individual)membersOfPop.retrieve();
         anIndividual.acceptVisitor(this);
	       aPopulation.updateMaxMinSumFitness(anIndividual);
        }

    };



}
