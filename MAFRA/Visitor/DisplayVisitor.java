

package MAFRA.Visitor;

import MAFRA.Population_Individual.*;


/** This Visitor class is responsable for moving through the population and displaying each  individual.

    @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */

public class DisplayVisitor extends Visitor {




  public void visitIndividual(Individual anIndividual)
    {
      anIndividual.display();
    };

  public void visitPopulation(Population aPopulation)
    {
     Individual anIndividual;
     long i;

     System.out.println("Population "+aPopulation.getName()+"|"+aPopulation.getMembersNumber()+"|");
      i=0;
      for( aPopulation.first( ); aPopulation.isInList( ); aPopulation.advance( ) )
        {
         anIndividual =(Individual)(aPopulation.retrieve());
    	    System.out.print("-----["+i+"]:");
         anIndividual.acceptVisitor(this);
    	    i = i+1;
        }

    };



}
