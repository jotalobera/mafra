



package MAFRA.Visitor;

import java.awt.*;
import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;



/** This Visitor class is responsable for moving through the population and displaying each  individual.

    @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */


public class MaxSATAWTDisplayVisitor extends AWTDisplayVisitor {

  private ProblemFactory myProblem;

  public MaxSATAWTDisplayVisitor(ProblemFactory aProblem)
    {
      super();
      myProblem = aProblem;
    }

  public void visitIndividual(Individual anIndividual)
    {
      anIndividual.display(myTextArea);
      myTextArea.append("  -----Sat="+myProblem.fitness(anIndividual)+"\n\n");
    };
  

}
