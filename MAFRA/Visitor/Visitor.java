

package MAFRA.Visitor;

import MAFRA.Population_Individual.*;


/** The Visitor class is the superclass of al the visitors that a GA might interact with 
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */


public class Visitor {

  
  public void visitIndividual(Individual anIndividual)
    {
    };

    public void visitPopulation(Population aPopulation)
      {
      };

}

