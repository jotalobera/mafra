




package MAFRA.Visitor;

import java.awt.*;
import MAFRA.Population_Individual.*;



/** This Visitor class is responsable for moving through the population and displaying each  individual.

    @version Memetic Algorithms Framework - V 1.0 - August 1999
    @author  Natalio Krasnogor
    */
public class AWTDisplayVisitor extends DisplayVisitor {

  protected TextArea myTextArea;
  protected Frame myFrame;

  public AWTDisplayVisitor()
    {
      super();

      myTextArea = new TextArea();
      myFrame = new Frame("Evolving...");
      myFrame.add(myTextArea);
      myFrame.show();

    }

  public void visitIndividual(Individual anIndividual)
    {
      anIndividual.display(myTextArea);
    };
  

  public void visitPopulation(Population aPopulation)
    {
     Individual anIndividual;
     long i;
     
     myTextArea.insert("Population "+aPopulation.getName()+"|"+aPopulation.getMembersNumber()+"| \n",myTextArea.getCaretPosition());
      i=0;   
      for( aPopulation.first( ); aPopulation.isInList( ); aPopulation.advance( ) )
        {
         anIndividual =(Individual)(aPopulation.retrieve());
      	 myTextArea.append("-----["+i+"]:");
         anIndividual.acceptVisitor(this);
	 i = i+1;
        }
    
    };

    public void close()
      {
      	myFrame.setVisible(false);
      	myFrame.dispose();
      }

}
