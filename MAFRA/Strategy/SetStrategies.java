


package MAFRA.Strategy;


/** This interface specifies the setting and reading methods for the many strategies used
    across the system. 
    @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public interface SetStrategies{
 /* SetStrategies Interface Definitions */

 /* Setting methods */
 public abstract void addSelectionStrategy(SelectionStrategy aS);

 public abstract void addLocalSearchStrategy(AbstractLocalSearchStrategy aS);

// public abstract  void addMutationStrategy(MutationStrategy aS);
 public abstract  void addMutationStrategy(AbstractMutationStrategy aS);

 public abstract  void addRestartPopStrategy(RestartPopStrategy aS);

 public abstract  void addInitializeStrategy(InitializePopStrategy aS);

 public abstract  void addFinalizationStrategy(FinalizationStrategy aS);

 // public abstract  void addMatingStrategy(MatingStrategy aS);
 public abstract  void addMatingStrategy(AbstractMatingStrategy aS);


 /* Reading methods */
 public abstract  SelectionStrategy getSelectionStrategy();

 public abstract  AbstractLocalSearchStrategy getLocalSearchStrategy();

// public abstract MutationStrategy getMutationStrategy();
 public abstract  AbstractMutationStrategy getMutationStrategy();

 public abstract  RestartPopStrategy getRestartPopStrategy();

 public abstract  InitializePopStrategy getInitializePopStrategy();

 public abstract  FinalizationStrategy getFinalizationStrategy();

 // public abstract  MatingStrategy getMatingStrategy();
 public abstract  AbstractMatingStrategy getMatingStrategy();

}
