
package MAFRA.Strategy;

import MAFRA.Executor.Executor;
import MAFRA.SelectionStrategy.*;

/** This class implements the updating procedures of the populations.
 @version Memetic Algorithms Framework - V 1.0 - August 1999
 @author  Natalio Krasnogor
 */


public class SelectionStrategy extends Strategy{


  public SelectionStrategy(Executor anExecutor)
    {
      super(anExecutor);
    }

  public void execute()
    {
      /* there is no speciall code yet */
      super.execute();
    }

  public SelectionStrategyExecutor getSelectionStrategyExecutor()
    {
      return (SelectionStrategyExecutor)executor;
    }

}
