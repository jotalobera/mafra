


package MAFRA.Strategy;



import java.util.Hashtable;
import MAFRA.Executor.Operator;
import MAFRA.AbstractEvolutionaryPlan.*;
import MAFRA.AbstractPlan.*;

/**
This is a very simple strategy for restarting the evolutionary plans being coevolved.
It does not use an executor (which can be added later for grater flexibility)
and through its execute method the strategy is directly executed.


 @version Memetic Algorithms Framework - V 1.0 - Setember 1999
 @author  Natalio Krasnogor
 */

public class RestartCoEvolvingPopStrategy extends Strategy implements Operator{


 private Hashtable myEvolutionaryPlans;

 public RestartCoEvolvingPopStrategy()
   {
     super(null);
     myEvolutionaryPlans = new Hashtable();
   }

 public void setArguments(Hashtable args)
   {
     
     int i;
     
     for (i=0;i<args.size();i++)
       {
	 myEvolutionaryPlans.put("evolutionaryPlan"+(i+1),(EvolutionaryPlan)args.get("evolutionaryPlan"+(i+1)));
       }


   }


 public Hashtable getResults()
   {
     return(null);
   }
 
 public void execute()
   {
     Plan aPlan;
     int i;


    for (i=0;i<myEvolutionaryPlans.size();i++)
      {
	aPlan = ((EvolutionaryPlan)(myEvolutionaryPlans.get("evolutionaryPlan"+(i+1)))).getPlan();
	aPlan.setGenerationsNow(0);
      }

   }
	


}
