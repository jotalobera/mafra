






package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Executor.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class implements the Local Search procedures of the populations.
It receives to arguments which are the population and 
the probability to apply local search to a given individual in the population.
The population is passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will apply local search to the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of applying local search to an individual.

 @version Memetic Algorithms Framework - V 1.0 - September 1999
 @author  Natalio Krasnogor
 */
public class LocalSearchStrategy4Individuals extends LocalSearchStrategy implements Operator{

 
 
  public LocalSearchStrategy4Individuals(LocalSearchFactory aLocalSearchFactory)
    {
     super(aLocalSearchFactory);
     previousTemperature = 0;
    }

  /** This are the methods from the Operator Interface. The Hashtable receives three associations,
      one with key="Population" and value the population to be localy searched and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to apply local search to a given individual of the population. The third one is the problem factory,
      that the local search strategy MUST know in order to operate.*/
  public void setArguments(Hashtable args)
    {
      myPopulation = (Population)(args.get("Population"));
      proLocSearPerInd = ((Double)args.get("ProbabilityPerIndividual")).doubleValue();
      myProblemFactory = ((ProblemFactory)args.get("ProblemFactory"));
      myFitnessVisitor = new FitnessVisitor(myProblemFactory);
    }
  
  
  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     Individual tmp;
     Individual tmp1st;
     double     rnd;
     Hashtable  args;
     Population tmpPop;
     double     temperature;
     double     delta;
     int        generationsWithoutChange=0;
     int        M = 10;
     double oldFitness;


     //     tmpPop = new Population();
     args       = new Hashtable();

     System.out.println("_____________________");
     /* We advance the iterator in such a way that the best individual is not modified, and we store
	in its temperature the temperature of its inmediate follower */
     myPopulation.first();
     tmp1st = (TourIndividual) myPopulation.retrieve();
     myPopulation.advance();
     tmp = (TourIndividual) myPopulation.retrieve();
     ((TourIndividual)tmp1st).setTemperature(     ((TourIndividual)tmp).getTemperature());
     ((TourIndividual)tmp1st).setNumberGenerationsWithoutChange(     ((TourIndividual)tmp).getNumberGenerationsWithoutChange());
     /* We start to optimize */
     for(;myPopulation.isInList();myPopulation.advance())
       {
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	 tmp = (Individual) myPopulation.retrieve();
         if (rnd<proLocSearPerInd)
	   { /* we decided to apply local search to the individual */
	     generationsWithoutChange = ((TourIndividual)tmp).getNumberGenerationsWithoutChange();
	     //	     delta      = M-generationsWithoutChange;
	     delta = (generationsWithoutChange+0.0)/(M+0.0);
	     if ((delta >=0)&&(delta<1))
	       {
	
		     temperature = delta;
		     previousTemperature = temperature;
	
	       }
	     else
	       {
		 previousTemperature = ((TourIndividual)tmp).getTemperature();
		 temperature = previousTemperature*2;
		 previousTemperature = temperature;
	       }
	     ((TourIndividual)tmp).setTemperature(previousTemperature);
	     System.out.println("Temperature = "+temperature);
	     args.put("Individual",tmp);
	     args.put("Temperature",new Double(temperature));
	     args.put("Distances",((TSPProblem)myProblemFactory).getDistances());
	     args.put("FitnessVisitor",myFitnessVisitor);
	     ((LocalSearchOperator)executor).setArguments(args);
	     oldFitness = tmp.getFitness();
             executor.execute();
	     if(oldFitness!= tmp.getFitness())
	       {
		 ((TourIndividual)tmp).setNumberGenerationsWithoutChange(0);
		 ((TourIndividual)tmp).setTemperature(0);
	       }
	     else
	       {
		 ((TourIndividual)tmp).setNumberGenerationsWithoutChange(((TourIndividual)tmp).getNumberGenerationsWithoutChange()+1);
	       }




	   }
	 /*	 else
	   {
	     ((TourIndividual)tmp).setNumberGenerationsWithoutChange(((TourIndividual)tmp).getNumberGenerationsWithoutChange()+1);
	   } */
	   
       }

    }


}
