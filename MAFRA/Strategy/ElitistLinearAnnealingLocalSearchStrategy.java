






package MAFRA.Strategy;


import java.util.Hashtable;
import MAFRA.Population_Individual.*;
import MAFRA.Executor.*;
import MAFRA.LocalSearcher.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;
import MAFRA.Visitor.*;

/** This class implements the Local Search procedures of the populations.
It receives to arguments which are the population and 
the probability to apply local search to a given individual in the population.
The population is passed by REFERENCE and need only be passed once.
Later, succesive calls to execute() methods will apply local search to the population
with the associated probability.
Subsequent calls to setArguments can be used to change the population reference or
the probability of applying local search to an individual.

 @version Memetic Algorithms Framework - V 1.2 - November 1999
 @author  Natalio Krasnogor
 */
public class ElitistLinearAnnealingLocalSearchStrategy extends AbstractLocalSearchStrategy implements Operator{

  protected double         previousTemperature;
  protected double         temperature;

  public ElitistLinearAnnealingLocalSearchStrategy(LocalSearchFactory aLocalSearchFactory)
    {
     super(new LocalSearchOperator(aLocalSearchFactory));
     previousTemperature = 0.0;
     temperature         = 2.0;
    }




  /** This are the methods from the Operator Interface. The Hashtable receives three associations,
      one with key="Population" and value the population to be localy searched and the other with
      key="ProbabilityPerIndividual" with a double value between 0 and 1, which is the pobability
      to apply local search to a given individual of the population. The third one is the problem factory,
      that the local search strategy MUST know in order to operate.*/
  public void setArguments(Hashtable args)
    {
      myPopulation = (Population)(args.get("Population"));
      proLocSearPerInd = ((Double)args.get("ProbabilityPerIndividual")).doubleValue();
      myProblemFactory = ((ProblemFactory)args.get("ProblemFactory"));
      myFitnessVisitor = new FitnessVisitor(myProblemFactory);
    }
  
 

  /** This function returns null always because the population is passed by reference. Hence no need for
      return values. */
  public Hashtable getResults()
    {
      return null;
    }

  public void execute()
    {
     Individual tmp;
     double     rnd;
     Hashtable  args;
     Population tmpPop;
     double     delta;


     //     tmpPop = new Population();
     previousTemperature = temperature;
     temperature = temperature * 0.98;

     myPopulation.first();
     tmp = (Individual) myPopulation.retrieve();
     ((TourIndividual)tmp).setTemperature(temperature);
     // this is for elitism when uncommented 
     myPopulation.advance();
     for(;myPopulation.isInList();myPopulation.advance())
       {
	 rnd =  MAFRA_Random.nextUniformDouble(0.0,1.0);
	 tmp = (Individual) myPopulation.retrieve();
	 ((TourIndividual)tmp).setTemperature(temperature);
         if (rnd<proLocSearPerInd)
	   { /* we decided to apply local search to the individual */
	     args       = new Hashtable();
	     args.put("Individual",tmp);
	     args.put("Temperature",new Double(temperature));
	     args.put("Distances",((TSPProblem)myProblemFactory).getDistances());
	     args.put("FitnessVisitor",myFitnessVisitor);
	     ((LocalSearchOperator)executor).setArguments(args);
             executor.execute();
	   }
	   
       }

    }


}
