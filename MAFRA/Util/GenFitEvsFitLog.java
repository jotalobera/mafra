package MAFRA.Util;

import MAFRA.Executor.*;
import MAFRA.AbstractPlan.*;
import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;

/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor


Instances of this class  log  the number of generations, the number of fitness evaluations and the current
maximum fitness in the population.
    */
public  class GenFitEvsFitLog extends Log{

  protected Plan   myPlan;
  protected ProblemFactory myProblem;

  protected Population thePopToBeLogged;

  public GenFitEvsFitLog(Plan aPlan,String aFileName,long aLogEveryGenerations, ProblemFactory aProb,String fileFormatComment)
    {
      super(aPlan.getGA(),aFileName,aLogEveryGenerations);
      myPlan           = aPlan;
      myProblem        = aProb;
      thePopToBeLogged = (aPlan.getGA()).getPopulation("parentsPopulation");
      if (fileFormatComment.equals(""))
    	{
    	  writeToFile("% Log Format: Gen# FitEvs# Fit \n");
    	}
          else
    	{
    	  writeToFile("% Log Format: "+fileFormatComment+" \n");
    	}
    }

  public void execute()
    {
      if((myPlan.getGenerationsNow() % logEveryGenerations)==0)
    	{
    	  writeLog();
    	}
    }


  public void writeLog()
    {
      String newLogString;

     newLogString = new String();
     newLogString = newLogString + myPlan.getGenerationsNow()+" "+myProblem.getNumberOfFitnessCalls() +" "+ thePopToBeLogged.getMaxFitness()+" \n";

     writeToFile(newLogString);

    }



  /** This method writes the number of generations, number of fitness evaluations, the maximum
      fitness in the population PLUS a string passed as an argument.*/
  protected void writeLog(String toBePadded)
    {
      String newLogString;

     newLogString = new String();
     newLogString = newLogString + myPlan.getGenerationsNow()+" "+myProblem.getNumberOfFitnessCalls() +" "+ thePopToBeLogged.getMaxFitness()+toBePadded+" \n";

     writeToFile(newLogString);

    }


}
