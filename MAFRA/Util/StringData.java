
package MAFRA.Util;

/** StringData is an interface used to specify a generic data about string encodings for the whole system.
@version Memetic Algorithms Framework - V 1.0 - July 1999
@author  Natalio Krasnogor
*/

public interface StringData{
 
  static final long  _MAXIMUN_ALPHABET_SIZE = 200;

  public void setAlphabetSize(long size);
  public void setVariableLength(boolean variableL);
  public void setStringLength(long lengthS);
  public void setSimbols(char simbols[]);
  public long getAlphabetSize();
  public char []getAlphabet();
  public boolean getVariableLength();
  public long getStringLength();
  public char getSimbol(long number);

 

}
