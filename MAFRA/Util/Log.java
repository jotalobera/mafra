package MAFRA.Util;

import java.io.*;

import MAFRA.Executor.*;
import MAFRA.AbstractPlan.*;

/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor

This is the super class of al the Log objects that save information onto a diskfile.
The information is taken from a population which is past as a paremeter
    */
public abstract class Log implements Executor
{

  protected GA myGA;
  private String parametersString;
  private String fileName;
  private PrintWriter fileToWrite;
  protected long logEveryGenerations;
  
  
  /** This constructor receives a GA instance from where it will obtain parameters (proX,proM,etc),
      a filename where it will store the logs and a long number which specifies in number of generations
      how often the log should be updated. */
  public Log(GA aGA,String aFileName,long aLogEveryGenerations)
    {
      myGA = aGA;

      /* we build the parametersString */
      parametersString = new String("% Run specifics: #generations Mu Lambda ProbX ProbM ProbLS \n");
      parametersString = parametersString+"%"+ ((Long)(myGA.getParameter("maxGenerationsNumber"))).longValue()+" ";
      parametersString = parametersString+( (myGA.getSelectionStrategy()).getSelectionStrategyExecutor()).getMu()+" ";
      parametersString = parametersString+( (myGA.getSelectionStrategy()).getSelectionStrategyExecutor()).getLambda()+" ";
      parametersString = parametersString+ (myGA.getMatingStrategy()).getProbabilityCrossOver()+" ";
      parametersString = parametersString+ (myGA.getMutationStrategy()).getProbabilityMutation()+" ";
      parametersString = parametersString+ (myGA.getLocalSearchStrategy()).getProbabilityLocalSearch()+" ";
      parametersString = parametersString+"\n";
      parametersString = parametersString+"% Logging every "+aLogEveryGenerations+" generations: \n";
      /* I should add here a DATE and TIME STAMP !!!!*/
      
      /* we open the file to write */
      fileName      = aFileName;
      try
	{
	  fileToWrite   = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));
	  writeToFile(parametersString);
	} 
      catch (IOException e)
	{
	  System.out.println("ERROR - couldn't open file "+fileName+ " in Log.java");
	  //	  System.exit(1);
	}
      
      logEveryGenerations = aLogEveryGenerations;
    }
 
  /** This method set the GA the log will work with. From it, it will obtain information
      such as populations size, probabilities of operators, etc */
  public void setGA(GA aGA)
    {
      myGA = aGA;
    }

  public GA getGA()
    {
      return  myGA;
    }

  public abstract void execute();


  /** This method is to stop logging data into a file */
  public void endLog()
    {
      fileToWrite.close();
    }

 protected void writeToFile(String aString)
 {
   fileToWrite.print(aString);
   fileToWrite.flush();
 } 

}
