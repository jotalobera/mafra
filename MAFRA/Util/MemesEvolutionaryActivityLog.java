
package MAFRA.Util;

import MAFRA.Executor.*;
import MAFRA.AbstractPlan.*;
import MAFRA.Population_Individual.*;
import MAFRA.Factory.*;
import MAFRA.Util.*;

/** @version Memetic Algorithms Framework - V 1.2 - February 2000
    @author  Natalio Krasnogor

Instances of this subclass of Log, write (besides all of the inmediate superclass) the evolutionary activity of the memes.
    */

public  class MemesEvolutionaryActivityLog extends Log{

  protected Population thePopToBeLogged;
  protected AbstractMemeProbConExpTable myHelpers;
  protected Plan myPlan;

  public MemesEvolutionaryActivityLog(Plan aPlan,String aFileName,long aLogEveryGenerations,String fileFormatComment,AbstractMemeProbConExpTable myTable)
    {
      super(aPlan.getGA(),aFileName,aLogEveryGenerations);
      myPlan = aPlan;
   thePopToBeLogged = (aPlan.getGA()).getPopulation("parentsPopulation");
      if (fileFormatComment.equals(""))
	{
	  writeToFile("% Log Format: Gen# FitEvs# Fit \n");
	}
      else
	{
	  writeToFile("% Log Format: "+fileFormatComment+" \n");
	}
      myHelpers = myTable;

    }
 
  public void execute()
    {
      String sToBePadd;
      double avgFitness;
      int i;
      int size;
      

      if((myPlan.getGenerationsNow() % logEveryGenerations)==0)
	{
          size = myHelpers.getNumberOfHelpers();	
	  avgFitness=thePopToBeLogged.getAvgFitness();
	  sToBePadd = " "+ myPlan.getGenerationsNow()+" "+avgFitness+" ";
	  for(i=0;i<size;i++)
	    {
	      sToBePadd = sToBePadd + myHelpers.getExpressionValue(i) +" ";
	    }
	    sToBePadd = sToBePadd +": ";
	  for(i=0;i<size;i++)
	    {
	      sToBePadd = sToBePadd + myHelpers.getEvolutionaryActivityValue(i) +" ";
	    }  
	    
	  sToBePadd = sToBePadd+"\n";


	  writeToFile(sToBePadd);

	}

    }


/* public void execute()
// This is the composition of cosine waves for each meme. Each meme is represented by a different odd frequency
   number
    {
      String sToBePadd;
      double avgFitness;
      int i;
      int size;
      double compo=0.0;
      long timeNow;
      
      timeNow = myPlan.getGenerationsNow();
      if(( timeNow % logEveryGenerations)==0)
	{
          size = myHelpers.getNumberOfHelpers();	
	  avgFitness=thePopToBeLogged.getAvgFitness();
	  sToBePadd = " "+ myPlan.getGenerationsNow()+" "+avgFitness+" ";
	  for(i=0;i<size;i++)
	    {
	     compo = ( myHelpers.getExpressionValue(i)+0.0)*Math.cos((2*i+1)*(timeNow+0.0)) + compo;
	    }
	     sToBePadd = sToBePadd + compo;
	  sToBePadd = sToBePadd+"\n";


	  writeToFile(sToBePadd);

	}

    }*/



} 
