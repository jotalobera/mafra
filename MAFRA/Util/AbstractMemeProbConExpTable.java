package MAFRA.Util;




import java.util.*;

import MAFRA.LocalSearcher.*;


public class AbstractMemeProbConExpTable {

  protected Object myTable[][];
  protected int freeSlot;
  protected int size;
  
  
  public AbstractMemeProbConExpTable(int numberOfItems)
   {
     myTable  = new Object[numberOfItems][7];
     /* myTable[i][0] contains a local searcher.
        myTable[i][1] contains a ls probability.
	myTable[i][2] empty slot for future use.
	myTable[i][3] empty slot for future use.
        myTable[i][4] contains the concentration of meme i (fixed in the static version = 1/numberOfItems).
	myTable[i][5] contains the expression value of meme i.
	myTable[i][6] contains the evolutionary activity of meme i. 
	*/
     freeSlot = 0;
     size     = numberOfItems;
   }



  public void setConcentrationValue(int helperId,double aVal)
   {
     myTable[helperId][4] =  (Object)(new Double(aVal));
   }


   public double getConcentrationValue(int helperId)
   {

     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][4]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMemeProbConExpTable size");
	 return(-1.0);
       }

   }

 public void setExpressionValue(int helperId, int aVal)
   {
     myTable[helperId][5] =  (Object)(new Integer(aVal));
   }

 public int getExpressionValue(int helperId)
   {

     if(helperId<freeSlot)
       {
	 return ( ((Integer)myTable[helperId][5]).intValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractMemeProbConExpTable size");
	 return(-1);
       }

   }


 public void setEvolutionaryActivityValue(int helperId,double aVal)
   {
     myTable[helperId][6] =  (Object)(new Double(aVal));
   }

 public double getEvolutionaryActivityValue(int helperId)
   {

     if(helperId<freeSlot)
       {
	 return (((Double)myTable[helperId][6]).doubleValue());
       }
     else
       {
	 System.out.println("ERROR - trying to read beyond AbstractLocalSearcherProbConExpTable size");
	 return(-1.0);
       }

   }


 
 public int getNumberOfHelpers()
   {
     return size;
   }

  
  

}


