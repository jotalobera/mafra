package MAFRA.Util;

public class Contact{

	private int x;
	private int y;
	private char residue;


	public Contact(int xValue, int yValue, char residueValue){
		x = xValue;
		y = yValue;
		residue = residueValue;
	}

	public int getLeftResidue(){
		return x;
	}

	public int getRightResidue(){
		return y;
	}

	public char getResidueType(){
		return residue;
	}
	
}
