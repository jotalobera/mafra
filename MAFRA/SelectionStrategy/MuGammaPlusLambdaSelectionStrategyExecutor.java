
package MAFRA.SelectionStrategy;

import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;


/** This class provides a (Mu+Lambda) Selection Strategy.
  @version Memetic Algorithms Framework - V 1.0 - August 1999
  @author  Natalio Krasnogor
  */

public class MuGammaPlusLambdaSelectionStrategyExecutor extends SelectionStrategyExecutor{
   
  private long gamma;

  public MuGammPlusLambdaSelectionStrategyExecutor(Population aParentsPop,Population anOffspringsPop, long mU, long gAMMA, long lAMBDA)
    {
      super(aParentsPop,anOffspringsPop,mU,lAMBDA);
      gamma = gAMMA
    }



 public void setGamma(long gAMMA)
   {
     gamma = gAMMA;
   }

 public long getGamma()
   {
     return gamma;
   }




  public void execute()
    {
      Population tmpPop;
      SortingVisitor aSortingVisitor;

      aSortingVisitor = new SortingVisitor();
      tmpPop          = new Population();
      tmpPop.setName(parentsPopulation.getName());
      parentsPopulation.copyTo(tmpPop,mu);

      offspringsPopulation.acceptVisitor(aSortingVisitor);
      offspringsPopulation.copyTo(tmpPop,lambda);

      /* sorts out in decreasing order( based on fitness) the population,
         first individual is fittest */
      tmpPop.acceptVisitor(aSortingVisitor);
      /* delete all individual in the population after the Mu-th one */

         parentsPopulation.extinct();
	 tmpPop.copyTo(parentsPopulation,mu);
	 parentsPopulation.setName(tmpPop.getName());



    }


}
