
package MAFRA.SelectionStrategy;

import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;


/** This class provides a (Mu+Lambda) Selection Strategy.
  @version Memetic Algorithms Framework - V 1.0 - August 1999
  @author  Natalio Krasnogor
  */

public class MuPlusLambdaDiversityPreservingSelectionStrategyExecutor extends SelectionStrategyExecutor{


  public MuPlusLambdaDiversityPreservingSelectionStrategyExecutor(Population aParentsPop,Population anOffspringsPop, long mU, long lAMBDA)
    {
      super(aParentsPop,anOffspringsPop,mU,lAMBDA);
    }


  public void execute()
    {
      Population tmpPop;
      SortingVisitor aSortingVisitor;
      int copied = 0;

      aSortingVisitor = new SortingVisitor();
      tmpPop          = new Population();
      tmpPop.setName(parentsPopulation.getName());
      parentsPopulation.copyTo(tmpPop,mu);

      offspringsPopulation.acceptVisitor(aSortingVisitor);
    //  System.out.println("|offs|="+offspringsPopulation.getMembersNumber()+", lambda="+lambda);
   
      
        for(offspringsPopulation.first();(offspringsPopulation.isInList())&&(copied<lambda);offspringsPopulation.advance())
       {
	if(tmpPop.addIndividualIfNotInYet( (Individual)(offspringsPopulation.retrieve()).copyIndividual()))
	{
	 copied++;
	}
	
       }
      /* sorts out in decreasing order( based on fitness) the population,
         first individual is fittest */
      tmpPop.acceptVisitor(aSortingVisitor);
      /* delete all individual in the population after the Mu-th one */

         parentsPopulation.extinct();
	 tmpPop.copyTo(parentsPopulation,mu);
	 parentsPopulation.setName(tmpPop.getName());



    }


}

