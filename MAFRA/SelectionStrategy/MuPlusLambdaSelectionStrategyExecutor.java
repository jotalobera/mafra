
package MAFRA.SelectionStrategy;

import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;


/** This class provides a (Mu+Lambda) Selection Strategy.
  @version Memetic Algorithms Framework - V 1.0 - August 1999
  @author  Natalio Krasnogor
  */

public class MuPlusLambdaSelectionStrategyExecutor extends SelectionStrategyExecutor{


  public MuPlusLambdaSelectionStrategyExecutor(Population aParentsPop,Population anOffspringsPop, long mU, long lAMBDA)
    {
      super(aParentsPop,anOffspringsPop,mU,lAMBDA);
    }


  public void execute()
    {
      Population tmpPop;
      SortingVisitor aSortingVisitor;

      aSortingVisitor = new SortingVisitor();
      tmpPop          = new Population();
      tmpPop.setName(parentsPopulation.getName());
      parentsPopulation.copyTo(tmpPop,mu);

      offspringsPopulation.acceptVisitor(aSortingVisitor);
      //  System.out.println("|offs|="+offspringsPopulation.getMembersNumber()+", lambda="+lambda);
      offspringsPopulation.copyTo(tmpPop,lambda);

      /* sorts out in decreasing order( based on fitness) the population,
         first individual is fittest */
      tmpPop.acceptVisitor(aSortingVisitor);
      /* delete all individual in the population after the Mu-th one */
      // System.out.println("######");
      //System.out.println(offspringsPopulation.getMinFitness()+","+offspringsPopulation.getMaxFitness()+"    "+parentsPopulation.getMinFitness()+","+parentsPopulation.getMaxFitness());
      parentsPopulation.extinct();
      tmpPop.copyTo(parentsPopulation,mu);
      //System.out.println(parentsPopulation.getMinFitness()+","+parentsPopulation.getMaxFitness());
      //System.out.println("######");
	    parentsPopulation.setName(tmpPop.getName());



    }


}
