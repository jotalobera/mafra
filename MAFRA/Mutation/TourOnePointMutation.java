



package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;



/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    */
public class TourOnePointMutation extends AbstractMutation{



 public Individual individualMutation(Individual parent1)
     {  /* double-bridge */

      int   c1[];
      int   rndPoint1;
      int   rndPoint2;
      int   long1;
      int   long2;
      int   segment[]; 
      int   size;
      int   half;
      int   f;
      int   i;
      
       

      c1       = (int[])(parent1.getChromosome());
      
      size     = (int)(parent1.getSize());
      half     = (int)(size/2)+1;
      
   
      rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)half);
      long1     = (int) MAFRA_Random.nextUniformLong(1,(long)(half-rndPoint1+1));
      rndPoint2 = (int) MAFRA_Random.nextUniformLong(half+1,(long)size); 
      long2     = (int) MAFRA_Random.nextUniformLong(1,(long)(size-rndPoint2+1));
      
      segment = new int[size];
      
      i=0;
      /*preSegment*/
      for(f=0;f<rndPoint1;f++)
      {
       segment[i]=c1[f];
       i++;
      }
     
      /*copy second segment to segment's one position*/
      for(f=rndPoint2;f<(rndPoint2+long2);f++)
      {
       segment[i]=c1[f];
       i++;
      }
      
      /*postSegment*/
      for(f=(rndPoint1+long1);f<rndPoint2;f++)
      {
       segment[i]=c1[f];
       i++;
      }
      
      /*copy first segment to segments's two position */
      for(f=rndPoint1;f<(rndPoint1+long1);f++)
      {
       segment[i]=c1[f];
       i++;
      }
      
      /*copy remaining parts */
      for(f=(rndPoint2+long2);f<size;f++)
      {
       segment[i]=c1[f];
       i++;
      }
      
      for(f=0;f<size;f++)
      {
       c1[f]=segment[f];
      }
     
       parent1.setChromosome(c1);
      ((TourIndividual)parent1).setTemperature(0.0);
      ((TourIndividual)parent1).setNumberGenerationsWithoutChange(0);


      return parent1;
      
    }


 public Individual individualMutation2(Individual parent1)
     {  /* two swap */

      int  c1[];
      int   rndPoint1;
      int   rndPoint2;
      int  rndMut; 
      
       

      c1       = (int[])(parent1.getChromosome());
      rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));
      rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));

	  rndMut        = c1[rndPoint1];
	  c1[rndPoint1]  = c1[rndPoint2];
	  c1[rndPoint2]= rndMut;

	


      parent1.setChromosome(c1);
      ((TourIndividual)parent1).setTemperature(0.0);
      ((TourIndividual)parent1).setNumberGenerationsWithoutChange(0);


      return parent1;
      
    }


/* this mutation implements a twoChange by means of an inversion (swaps two links, not to cities as
   twoSwap */ 
 public Individual individualMutation4(Individual indi)
 {//twoChange
     int startInversion;
     int endInversion;
     int maxI;
     int size;
     int tmp;
     int c[];
     int i;
     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion =(int)MAFRA_Random.nextUniformLong(0,size-2);
     endInversion =(int)MAFRA_Random.nextUniformLong(0,size-1);
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     maxI = (int)((endInversion-startInversion)/2);
     for(i=0;i<maxI;i++)
     {
      tmp=c[startInversion+i];
      c[startInversion+i]=c[endInversion-i];
      c[endInversion-i]=tmp;
     }


     indi.setChromosome(c);
     ((TourIndividual)indi).setTemperature(0.0);
     ((TourIndividual)indi).setNumberGenerationsWithoutChange(0);
     return (indi);
 }


 /* 1 city insertion */
 public Individual individualMutatio3(Individual parent1)
 
    {
      int   c1[];
      int   rndPoint1;
      int   rndPoint2;
      int   tmp; 
      int   i;
      
       

      c1       = (int[])(parent1.getChromosome());
      rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));
      rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));

      if(rndPoint2<rndPoint1)
	{
	  tmp       = rndPoint1;
	  rndPoint1 = rndPoint2;
	  rndPoint2 = tmp;
	}
	
      tmp = c1[rndPoint2];
      for(i=rndPoint2;i>rndPoint1;i--)
	{
	  c1[i] = c1[i-1];
	}
      c1[rndPoint1]=tmp;

      parent1.setChromosome(c1);
      ((TourIndividual)parent1).setTemperature(0.0);
      ((TourIndividual)parent1).setNumberGenerationsWithoutChange(0);



      return parent1;
      
    }

}
