



package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;
import java.util.BitSet;


/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */
public class BitSetOnePointMutation extends AbstractBitSetMutation{


 public Individual individualMutation(Individual parent1)
 
    {
      BitSet c1;
      char temp[];
      long   rndPoint;
      long   rndMut; 
      
       

      c1       = (BitSet)parent1.getChromosome();
      rndPoint = MAFRA_Random.nextUniformLong(0,(long)(parent1.getSize()));
      if(c1.get((int)rndPoint))
	{
	  c1.clear((int)rndPoint);
	}
      else
	{
	  c1.set((int)rndPoint);
	}
      parent1.setChromosome(c1);


      return parent1;
      
    }

}
