



package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;



/** @version Memetic Algorithms Framework - V 1.2 - October 1999
    @author  Natalio Krasnogor
    M3012-2
    */
public class EdgeEncodingOnePointMutation extends AbstractMutation{

 /* double-bridge move Johnson&McGeogh 1997 */
 public Individual individualMutation2(Individual parent1)
 {
  int c1[];
  int tmp1,tmp2,tmp3,tmp4;
  int rndPoint1,rndPoint2,rndPoint3,rndPoint4;
  int size=0;
 
  boolean allDifferent;
 
       
  allDifferent = false;
  c1           = (int[])(parent1.getChromosome());
  size         = (int)(parent1.getSize());
 
    do
      {
       rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       rndPoint3 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       rndPoint4 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       
       allDifferent = (rndPoint1!=rndPoint2) && (rndPoint1!=rndPoint3) && (rndPoint1!=rndPoint4) && (rndPoint2!=rndPoint3) && (rndPoint2!=rndPoint4) && (rndPoint3!=rndPoint4);
       
      allDifferent= allDifferent && (c1[rndPoint1]!=rndPoint3) && (c1[rndPoint2]!=rndPoint4) && (c1[rndPoint3]!=rndPoint1) && (c1[rndPoint4]!=rndPoint2);
	 


   }while(!allDifferent);

      
    tmp1=c1[rndPoint1];
    tmp2=c1[rndPoint2];
    tmp3=c1[rndPoint3];
    tmp4=c1[rndPoint4];
    
    c1[rndPoint1]=tmp3;
    c1[rndPoint2]=tmp4;
    c1[rndPoint3]=tmp1;
    c1[rndPoint4]=tmp2;
   
    parent1.setChromosome(c1);
    ((TourIndividual)parent1).setTemperature(0.0);
    ((TourIndividual)parent1).setNumberGenerationsWithoutChange(0);


     return parent1;
      
  
  
 
 }




 /* inversion */
  public Individual individualMutation(Individual parent1)
 
    {
      int  c1[];
      int  c2[];
      int  rndPoint1;
      int  rndPoint2;
      int  size; 
      boolean allDifferent;
       
      allDifferent = false;
      c1           = (int[])(parent1.getChromosome());
      size         = (int)(parent1.getSize());
      c2           = new int[size];
      do
      {
       rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       
       allDifferent = (rndPoint1!=rndPoint2);
	 
      }while(!allDifferent);
      
      c2 = edgeEncoding2PermEncoding(c1,size);
      invertion(c2,rndPoint1,rndPoint2);
      allDifferent = false;
      do
      {
       rndPoint1 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       rndPoint2 = (int) MAFRA_Random.nextUniformLong(0,(long)size);
       
       allDifferent = (rndPoint1!=rndPoint2);
	 
      }while(!allDifferent);
      
      invertion(c2,rndPoint1,rndPoint2);
      c1 = permEncoding2EdgeEncoding(c2,size);


      parent1.setChromosome(c1);
      ((TourIndividual)parent1).setTemperature(0.0);
      ((TourIndividual)parent1).setNumberGenerationsWithoutChange(0);


      return parent1;
      
    }

 private int[] edgeEncoding2PermEncoding(int c[],int size)
 {
  int i;
  int c2[];
  int s;
  int e;
  
  c2 = new int[size];
  
  s=0;
  e=c[s];
  for(i=0;i<size;i++)
  {
   c2[s]=e;
   s=e;
   e=c[s];
  }
  return c2;
 }

 private int[] permEncoding2EdgeEncoding(int aTour[], int size)
 {
  int i;
  int finalTour[];
  
  finalTour = new int[size];
  
  for(i=0;i<(size-1);i++)
  {
   finalTour[aTour[i]]=aTour[i+1];
  }
  finalTour[aTour[(int)size-1]]=aTour[0];
  return finalTour;    
 }


 private int[] invertion(int c[],int start,int end)
 
    {
      int   tmp; 
      int   i;
      int   l;
      
      
      if(end<start)
	{
	  tmp   = start;
	  start = end;
	  end   = tmp;
	}
      l = (int)((end-start)/2);	
     
      for(i=0;i<=l;i++)
	{
	  tmp         = c[start+i];
	  c[start+i]  = c[end-i];
	  c[end-i]    = tmp;
	}

      return c;
      
    }

}
