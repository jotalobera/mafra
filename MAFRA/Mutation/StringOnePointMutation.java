

package MAFRA.Mutation;

import MAFRA.Population_Individual.*;
import MAFRA.Util.*;

/** @version Memetic Algorithms Framework - V 1.0 - July 1999
    @author  Natalio Krasnogor
    */

public class StringOnePointMutation extends AbstractStringMutation{

 private boolean mutateTwoConsecutiveAsOne ;

 public Individual individualMutation(Individual parent1)
 
    {
      String p1;
      String c1;
      char temp[];
      long   rndPoint;
      long   rndMut; 
      
       

      p1       = (String)(parent1.getChromosome());
      temp     = p1.toCharArray();
      if( mutateTwoConsecutiveAsOne )
      {
       rndPoint = MAFRA_Random.nextUniformLong(0,p1.length()-1);
       rndMut   = MAFRA_Random.nextUniformLong(0,getAlphabetSize());
       temp[(int)rndPoint]= getSimbol(rndMut);
       rndPoint++;
       rndMut   = MAFRA_Random.nextUniformLong(0,getAlphabetSize());
       temp[(int)rndPoint]= getSimbol(rndMut);
      }
      else
      {
       rndPoint = MAFRA_Random.nextUniformLong(0,p1.length());
       rndMut   = MAFRA_Random.nextUniformLong(0,getAlphabetSize());
       temp[(int)rndPoint]= getSimbol(rndMut);
      }
      c1 = String.copyValueOf(temp);
      parent1.setChromosome(c1);
      return parent1;
      
    }
    
  public void mutateTwoConsecutiveAsOne(boolean aB)
  {
   mutateTwoConsecutiveAsOne = aB;
  }

}
