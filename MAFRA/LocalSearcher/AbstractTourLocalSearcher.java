

package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;

/** This is a membership class which is superclass of all local searchers under a Tour encoding.
    @version Memetic Algorithms Framework - V 1.0 - September 1999
    @author  Natalio Krasnogor
    */
public abstract class AbstractTourLocalSearcher extends AbstractLocalSearcher{



 public abstract Individual individualLocalSearch(Individual parent1, double temperature, double sigma,double popSize, FitnessVisitor aFitnessVisitor);


 public abstract Individual individualLocalSearch(Individual parent1, double temperature, double distances[][],FitnessVisitor aFitnessVisitor);

 public abstract Individual individualLocalSearch(Individual parent1, double temperature, long distances[][],FitnessVisitor aFitnessVisitor);


 public abstract Individual individualLocalSearch(Individual parent1, double temperature, double K, double distances[][],FitnessVisitor aFitnessVisitor);

}
