


package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;
import MAFRA.Factory.*;


/** @version Memetic Algorithms Framework - V 1.2 - August 2000
    @author  Natalio Krasnogor
    
    
    This class implements a general hill climber local searcher.
    At creation it receives:
    
    1) a boolean, SinOpt, such that if SinOpt=true then the algorithm will be
       a move-Opt, that is, it will try to apply move until no improvement can be found.
       If SinOpt=false, only a specified number of applications of move will be done.
    2) an int, blindFirstBest, such that if blindFirstBest=0 then a blind (random) application
       of Move will occurr. If blindFirstBest=1 the first improvement will
       be accepted, otherwise (blindFirstBest=2) then the best improvement will be accepted.
    3) an int, NumIt, that specifies the number of iterations. This is valid only when
       SinOpt = false.
    4) a move which is a string of the form:   A-->C where |A|=|C| and A,C are in Alphabet^*. A will be changed
       to C. There are  special rules:
       
        IdRule :  _-->_   that preserves the structure integrity.
	LRandom:  *-->P   where |*|=|P|, P is a fixed pattern that is applied to any pattern * of size |P|.
	RRandom:  P-->*   where |*|=|P|, P is a fixed pattern that is replaced by a random pattern * of size |P|.
	Random :  *-->*   where a random pattern of a given size replaces any pattern of the same size.
	Stretch:  *-->|   where any pattern * of size |*| is stretched. 
	Pivot1 :  *-->!1  where any pattern * of size |*| is pivoted once anticlockwise
	Pivot2 :  *-->!2  where any pattern * of size |*| is pivoted twice anticlockwise
	Pivot3 :  *-->!3  where any pattern * of size |*| is pivoted thrice anticlockwise
	Pivot4 :  *-->!4  where any pattern * of size |*| is pivoted fourth anticlockwise
	Pivot5 :  *-->!5  where any pattern * of size |*| is pivoted fifth anticlockwise
	Reflec1:  *-->%NS any pattern is reflected in the NS axis
	Reflec2:  *-->%ns any pattern is reflected in the ns axis
	Reflec3:  *-->%WE any pattern is reflected in the WE axis
    
	
     5) a boolean, HcBhc, such that if HcBhc=false then the points 1,2,3 and 4 above are performed
        in a strictly HillClimber manner. If, however, HcBhc=true then 1,2,3 and 4 are interpreted
        and performed as a BoltzmannHillclimber.
    */
public class PFGeneralHillClimberLocalSearcher extends AbstractLocalSearcher{

 private boolean sinOpt;
 private int blindFirstBest;
 private int numIt;
 private String lMove;
 private String rMove;
 private boolean hcBhc;
 private ProblemFactory myProblem;
 private FitnessVisitor myFitnessVisitor;
 private double temperature;
 private double K;
 private double errorThreshold;
 private int forbidTable[][];
 private int fPos;

 public PFGeneralHillClimberLocalSearcher(boolean SinOpt,boolean HcBhc, int BlindFirstBest, int NumIt, String Move, ProblemFactory aProblemFactory)
 {
  sinOpt           = SinOpt;
  hcBhc            = HcBhc;
  blindFirstBest   = BlindFirstBest;
  numIt            = NumIt;
  lMove            = parseLeftPart(Move); 
  rMove            = parseRightPart(Move);
  myProblem        = aProblemFactory; 
  myFitnessVisitor = new FitnessVisitor(myProblem);
  forbidTable      = new int[2][NumIt];
  fPos             = 0;
  }

 

 public Individual individualLocalSearch(Individual parent1)
 {
  if (parent1==null) 
  {
   System.out.println("ERROR - individual in local searcher PFGeneralHillClimberLocalSearcher is null");
   System.exit(0);
  }
  
  
  ((SelfAdaptingStaticPFStringIndividual)parent1).setTemperature(temperature);
  if(!(lMove.equals("_")) && !(rMove.equals("_"))) /* if the rule is the IdRule we do nothing*/
  {
   if(sinOpt)
   {/* perform an Opt-move search*/
    /* to be done later */
   }
   else
   {/* perform an interative search with numIt iterations */
    switch(blindFirstBest)
    {
     case 0: /* blind search */
     {
      if(!hcBhc) /* must be a strict hill-climber, we put the temperature to zero */
      {
       ((SelfAdaptingStaticPFStringIndividual)parent1).setTemperature(0);
       temperature = 0.0;
       boltzmannHillClimberBlindAscent(parent1,numIt,lMove,rMove);
      }
      else
      {
       boltzmannHillClimberBlindAscent(parent1,numIt,lMove,rMove);
      }
      break; 
     }
     case 1: /* first ascent */
     {
      if(!hcBhc) /* must be a strict hill-climber, we put the temperature to zero */
      {
       ((SelfAdaptingStaticPFStringIndividual)parent1).setTemperature(0);
       temperature = 0.0;
       boltzmannHillClimberFirstAscent(parent1,numIt,lMove,rMove);
      }
      else
      {
        boltzmannHillClimberFirstAscent(parent1,numIt,lMove,rMove);
      }
      break;
     }
     case 2: /* best ascent */
     {
      break;
     }
    }
   }     	    
  }/* de la prueba por IdRule */
  
  return(parent1);
  
 }


private void boltzmannHillClimberFirstAscent(Individual parent1,int numIt,String lMove,String rMove)
{ 
 int f;
 int its;
 int structureLength;
 int rMoveLength=0;
 int pivotPosition;
 int rndPivot;
 String newStructure=new String();
 double newFitness;
 String oldStructure;
 double oldFitness;
 double delta;
 double threshold;
 double rndNumber;
 char   randomMove[];
 boolean pivotRuleToBeProcessed = false;
 boolean reflectRuleToBeProcessed = false;
 boolean changed;
 boolean improvementFound;
 boolean macroMutRule = false;
 boolean pivotReflRul = false;
 double bestFitness=Double.NEGATIVE_INFINITY;
 String bestStructure=new String();
 int    rMoveLengthTable[]={3,6,12,18};
 
 
 

 oldStructure = (String)(((SelfAdaptingStaticPFStringIndividual)parent1).getChromosome());
 structureLength = oldStructure.length();
 oldFitness   = parent1.getFitness();
 rMoveLength  = rMove.length();
 
 //System.out.println("Start-LocalSearch:");  
 if(lMove.equals("*")&&(rMove.equals("*")||rMove.equals("|")))
 {/* ########### MACRO MUTATION RULE ############# */
  /* ###########         or          ############# */
  /* ###########    STRETCH RULE     ############# */
  
   changed = true;
   scramble(rMoveLengthTable,4);
   for (its=0;((its<numIt)&&changed);its++)
   {
    changed     = false;
    bestFitness = parent1.getFitness();
  //  rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength));
  //  rMoveLength= (int)( MAFRA_Random.nextUniformLong(2,10));
    rMoveLength= rMoveLengthTable[mod(its,4)];
    //System.out.println("ITS="+its+","+rMoveLength);
    if(macroMutRule)
    {/* MACRO MUT RULE */
     rMove = ((PFProblem)myProblem).getSubstructure((long)rMoveLength);
    }
    else
    {/* STRETCH RULE */
     rMove = ((PFProblem)myProblem).getStretchSubstructure((long)rMoveLength);
    }
    for(pivotPosition=0;pivotPosition<(structureLength-rMoveLength);pivotPosition++)
    {
     newStructure = oldStructure.substring(0,pivotPosition)+rMove+oldStructure.substring(pivotPosition+rMoveLength);
     parent1.setChromosome(newStructure);
     myFitnessVisitor.visitIndividual(parent1);
     newFitness = parent1.getFitness();
     if(bestFitness>=newFitness)
     {/* the meme is harmful so we do a Boltzmann acceptance criteria */
      delta      = bestFitness - newFitness;   
      threshold  = Math.exp(-K*delta/temperature);
      rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
      if(rndNumber<threshold)
      {/* the meme is not good but we accept it */
   //    System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
       bestFitness = newFitness;
       bestStructure = new String(newStructure);
       changed = true;

      }
     }
     else
     {/* the meme is good so we accept it */
     // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
      bestFitness = newFitness;
      bestStructure = new String(newStructure);
      changed = true;
     }
     
    }
    if (changed)
    {
     parent1.setChromosome(bestStructure);
     parent1.setFitness(bestFitness);
     oldStructure = new String(bestStructure);
    }
    else
    {
     parent1.setChromosome(oldStructure);
     parent1.setFitness(oldFitness);
     //oldStructure = new String(bestStructure);
    }
   }
   
 }
 else
 if(lMove.equals("*")&& (rMove.equals("%NS") || rMove.equals("%ns") || rMove.equals("%WE") || rMove.equals("!1") || rMove.equals("!2") || rMove.equals("!3") || rMove.equals("!4") || rMove.equals("!5")   ))
 {/* ################ REFLECT RULES ################# */
  /* ################      or       ################# */
  /* ################ PIVOT RULES   ################# */
  
   changed = true;
   scramble(rMoveLengthTable,4);
   for (its=0;((its<numIt)&&changed);its++)
   {
    changed     = false;
    bestFitness = parent1.getFitness();
    //rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength));
    //rMoveLength= (int)( MAFRA_Random.nextUniformLong(2,10));
    rMoveLength= rMoveLengthTable[mod(its,4)];
    //System.out.println("ITS="+its+","+rMoveLength);
    for(pivotPosition=0;pivotPosition<(structureLength-rMoveLength);pivotPosition++)
    {
     if(lMove.equals("*") && rMove.equals("%NS"))
     {/* this is Reflec1 rule */
      newStructure = ((PFProblem)myProblem).getReflectSubstructure(oldStructure,pivotPosition,rMoveLength,1);
      pivotReflRul=true;
     }
    else
    if(lMove.equals("*") && rMove.equals("%ns"))
     {/* this is Reflec2 rule */
      newStructure = ((PFProblem)myProblem).getReflectSubstructure(oldStructure,pivotPosition,rMoveLength,2);
      pivotReflRul=true;
     }
    else
    if(lMove.equals("*") && rMove.equals("%WE"))
     {/* this is Reflec3 rule */
      newStructure = ((PFProblem)myProblem).getReflectSubstructure(oldStructure,pivotPosition,rMoveLength,3);
      pivotReflRul=true;
     }
    else
    if(lMove.equals("*") && rMove.equals("!1"))
    { /* this is Pivot1 rule */
      newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,structureLength-pivotPosition,1);     
      pivotReflRul=false;
    }
    else
    if(lMove.equals("*") && rMove.equals("!2"))
    { /* this is Pivot2 rule */

      newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,structureLength-pivotPosition,2);     
      pivotReflRul=false;
    }
    if(lMove.equals("*") && rMove.equals("!3"))
    { /* this is Pivot3 rule */
      newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,structureLength-pivotPosition,3);     
      pivotReflRul=false;
    }
    else
    if(lMove.equals("*") && rMove.equals("!4"))
    { /* this is Pivot4 rule */
      newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,structureLength-pivotPosition,4);     
      pivotReflRul=false;
    }
    else
    if(lMove.equals("*") && rMove.equals("!5"))
    { /* this is Pivot5 rule */
      newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,structureLength-pivotPosition,5);     
      pivotReflRul=false;
    }
     parent1.setChromosome(newStructure);
     myFitnessVisitor.visitIndividual(parent1);
     newFitness = parent1.getFitness();
     if(bestFitness>=newFitness)
     {/* the meme is harmful so we do a Boltzmann acceptance criteria */
      delta      = bestFitness - newFitness;   
      threshold  = Math.exp(-K*delta/temperature);
      rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
      if(rndNumber<threshold)
      {/* the meme is not good but we accept it */
    //   System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
       bestFitness = newFitness;
       bestStructure = new String(newStructure);
       changed = true;

      }
     }
     else
     {/* the meme is good so we accept it */
     // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
      bestFitness = newFitness;
      bestStructure = new String(newStructure);
      changed = true;
     }
     
    }
    if (changed)
    {
     parent1.setChromosome(bestStructure);
     parent1.setFitness(bestFitness);
     oldStructure = new String(bestStructure);
    }
    else
    {
     parent1.setChromosome(oldStructure);
     parent1.setFitness(oldFitness);
     //oldStructure = new String(bestStructure);
    }
   }
  
    
   
  }
 else
 if( !lMove.equals("*") && !rMove.equals("*") )
 {/* ################   P-->P RULES ##################### */
   changed = true;
   for (its=0;((its<numIt)&&changed);its++)
   {
    changed     = false;
    bestFitness = parent1.getFitness();
    pivotPosition  = -1;
    do
    {
     pivotPosition = oldStructure.indexOf(lMove,pivotPosition+1);
     if((pivotPosition>-1)&&(pivotPosition+rMoveLength<structureLength))
     {
      newStructure = oldStructure.substring(0,pivotPosition)+rMove+oldStructure.substring(pivotPosition+rMoveLength);
      parent1.setChromosome(newStructure);
      myFitnessVisitor.visitIndividual(parent1);
      newFitness = parent1.getFitness();
      if(bestFitness>=newFitness)
      {/* the meme is harmful so we do a Boltzmann acceptance criteria */
       delta      = bestFitness - newFitness;   
       threshold  = Math.exp(-K*delta/temperature);
       rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
       if(rndNumber<threshold)
       {/* the meme is not good but we accept it */
     //    System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
        bestFitness = newFitness;
        bestStructure = new String(newStructure);
        changed = true;

       }
      }
      else
      {/* the meme is good so we accept it */
      // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
       bestFitness = newFitness;
       bestStructure = new String(newStructure);
       changed = true;
      }
     }
    } while( (pivotPosition<(structureLength-rMove.length())) &&(pivotPosition>-1) );
    if (changed)
    {
     parent1.setChromosome(bestStructure);
     parent1.setFitness(bestFitness);
     oldStructure = new String(bestStructure);
    }
    else
    {
     parent1.setChromosome(oldStructure);
     parent1.setFitness(oldFitness);
     //oldStructure = new String(bestStructure);
    }
   }
   
  
  }/* normal P-->P rule */
  else
  { /* *-->P rule */
   changed = true;
   for (its=0;((its<numIt)&&changed);its++)
   {
    changed     = false;
    bestFitness = parent1.getFitness();
    for(pivotPosition=0;pivotPosition<(structureLength-rMoveLength);pivotPosition++)
    {
     newStructure = oldStructure.substring(0,pivotPosition)+rMove+oldStructure.substring(pivotPosition+rMoveLength);
     parent1.setChromosome(newStructure);
     myFitnessVisitor.visitIndividual(parent1);
     newFitness = parent1.getFitness();
     if(bestFitness>=newFitness)
     {/* the meme is harmful so we do a Boltzmann acceptance criteria */
      delta      = bestFitness - newFitness;   
      threshold  = Math.exp(-K*delta/temperature);
      rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
      if(rndNumber<threshold)
      {/* the meme is not good but we accept it */
   //    System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
       bestFitness = newFitness;
       bestStructure = new String(newStructure);
       changed = true;

      }
     }
     else
     {/* the meme is good so we accept it */
     // System.out.println("BFitness="+bestFitness+"     NFitness="+newFitness);
      bestFitness = newFitness;
      bestStructure = new String(newStructure);
      changed = true;
     }
     
    }
    if (changed)
    {
     parent1.setChromosome(bestStructure);
     parent1.setFitness(bestFitness);
     oldStructure = new String(bestStructure);
    }
    else
    {
     parent1.setChromosome(oldStructure);
     parent1.setFitness(oldFitness);
     //oldStructure = new String(bestStructure);
    }
   }
   
  }
 // System.out.println("OldFitness="+oldFitness+", NewFitness="+parent1.getFitness()); 
 // System.out.println("End-LocalSearch.");
  
 }/* of method */
 











 





private void boltzmannHillClimberBlindAscent(Individual parent1,int numIt,String lMove,String rMove)
{ 
 int f;
 int its;
 int structureLength;
 int rMoveLength=0;
 int pivotPosition;
 String newStructure=new String();
 double newFitness;
 String oldStructure;
 double oldFitness;
 double delta;
 double threshold;
 double rndNumber;
 char   randomMove[];
 boolean pivotRuleToBeProcessed = false;
 boolean reflectRuleToBeProcessed = false;

 
 oldStructure = (String)(((SelfAdaptingStaticPFStringIndividual)parent1).getChromosome());
 structureLength = oldStructure.length();
 oldFitness   = parent1.getFitness();
 
 if ( lMove.equals(rMove) && lMove.equals("*"))
 {/* we apply a random macromutation, this is the Random rule */
  rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength));
  rMove = ((PFProblem)myProblem).getSubstructure((long)rMoveLength);
  pivotRuleToBeProcessed = false;  
  reflectRuleToBeProcessed = false;
 }
 else
 {
  if(lMove.equals("*") && rMove.equals("|")) 
  {/* this is the stretch rule */
    rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength));
    rMove = ((PFProblem)myProblem).getStretchSubstructure((long)rMoveLength);
    pivotRuleToBeProcessed = false;
    reflectRuleToBeProcessed = false;
  }
  else
  {
   if(lMove.equals("*") && (rMove.equals("!1") || rMove.equals("!2") || rMove.equals("!3") || rMove.equals("!4") || rMove.equals("!5") ) )
   {
    pivotRuleToBeProcessed = true;
   }
   else
   if(lMove.equals("*")&& (rMove.equals("%NS") || rMove.equals("%ns") || rMove.equals("%WE") ))
   {
    reflectRuleToBeProcessed = true;
   }
   else
   {
    rMoveLength  = rMove.length();
    pivotRuleToBeProcessed = false;
    reflectRuleToBeProcessed = false;
   }
  }     
 }
    
 
 for (its=0;its<numIt;its++)
 { 
 
  
   if(pivotRuleToBeProcessed)
   { 
  //  System.out.println("PIVOT");
    if(lMove.equals("*") && rMove.equals("!1"))
    {/* this is Pivot1 rule */
     pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
     rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
     //rMoveLength= (int)(structureLength-pivotPosition);
     newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,rMoveLength,1);

    }
    else
    {
     if(lMove.equals("*") && rMove.equals("!2"))
     {
      /* this is Pivot2 rule */
      pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
      rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
      //rMoveLength= (int)(structureLength-pivotPosition);
      newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,rMoveLength,2);     
     }
     else
     {
      if(lMove.equals("*") && rMove.equals("!3"))
      {
       /* this is Pivot3 rule */
       pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
       rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
       //rMoveLength= (int)(structureLength-pivotPosition);
       newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,rMoveLength,3);     
      }
      else
      {
       if(lMove.equals("*") && rMove.equals("!4"))
       {
        /* this is Pivot4 rule */
        pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
        //rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
        newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,rMoveLength,4);     
       }
       else
       {
        if(lMove.equals("*") && rMove.equals("!5"))
        {
         /* this is Pivot5 rule */
         pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
         rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
	 //rMoveLength= (int)(structureLength-pivotPosition);
         newStructure = ((PFProblem)myProblem).getPivotSubstructure(oldStructure,pivotPosition,rMoveLength,5);     
        }
       }
      }
     }
    }
   }/* of pivotRuleToBeProcessed */  
   else
   if(reflectRuleToBeProcessed)
    {/* we will use the same name of variables as for pivot although they mean different things here */
   //  System.out.println("REFLECT");
     if(lMove.equals("*") && rMove.equals("%NS"))
     {/* this is Reflec1 rule */
      pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
      rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
      //rMoveLength= (int)(structureLength-pivotPosition);
      newStructure = ((PFProblem)myProblem).getReflectSubstructure(oldStructure,pivotPosition,rMoveLength,1);
     }
     else
     if(lMove.equals("*") && rMove.equals("%ns"))
     {/* this is Reflec2 rule */
      pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
      rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
      //rMoveLength= (int)(structureLength-pivotPosition);
      newStructure = ((PFProblem)myProblem).getReflectSubstructure(oldStructure,pivotPosition,rMoveLength,2);
     }
     else
     if(lMove.equals("*") && rMove.equals("%WE"))
     {/* this is Reflec3 rule */
      pivotPosition= (int)( MAFRA_Random.nextUniformLong(0,structureLength-1));
      rMoveLength= (int)( MAFRA_Random.nextUniformLong(0,structureLength-pivotPosition));
      //rMoveLength= (int)(structureLength-pivotPosition);
      newStructure = ((PFProblem)myProblem).getReflectSubstructure(oldStructure,pivotPosition,rMoveLength,3);
     }
    }
    else
    if((!pivotRuleToBeProcessed) && (!reflectRuleToBeProcessed))
    { 
        // System.out.println("STRETCH or MACRO");
     f            =(int)( MAFRA_Random.nextUniformLong(0,structureLength-rMoveLength));   
     newStructure = oldStructure.substring(0,f)+rMove+oldStructure.substring(f+rMoveLength);
    }
   
  parent1.setChromosome(newStructure);
  myFitnessVisitor.visitIndividual(parent1);
  newFitness = parent1.getFitness();

 
 
  delta      = oldFitness - newFitness;
  if(delta<0)
  {/* it is an improvement */
  // System.out.println("GOOD | oldFitness="+oldFitness+", newFitness="+newFitness);
   oldStructure = (String)(((SelfAdaptingStaticPFStringIndividual)parent1).getChromosome());
   oldFitness   = parent1.getFitness();
  }
  else
  {
   threshold  = Math.exp(-K*delta/temperature);
   rndNumber  = MAFRA_Random.nextUniformDouble(0.0,1.0);
   //System.out.println("K="+K+", delta="+delta+", temperature="+temperature+", threshold="+threshold+", rndNumber="+rndNumber);
   if(rndNumber<=threshold)/* carefull 'cos threshold sometimes get NaN value and hence this comparison will be false*/
   {
    /*  we accept the move even though it is a bad one */
    //System.out.println("BAD  | oldFitness="+oldFitness+", newFitness="+newFitness);
    oldStructure = (String)(((SelfAdaptingStaticPFStringIndividual)parent1).getChromosome());
    oldFitness   = parent1.getFitness();
   }
   else
   {
   /* we reject the move  and restore the previous structure */
    parent1.setChromosome(oldStructure);
    parent1.setFitness(oldFitness);

   }
  }
 }
 
 


}








/* ############################################### */
/* General           auxiliary             methods */
/* ############################################### */

int mod(int a, int b)
{
// System.out.print("mod("+a+","+b+")=");
 if (a>=0)
 {
 // System.out.println((b+a)%b);
  return (a%b);
 }
 else
 {
  while(b+a<0)
  {
   a=b+a;
  }
 // System.out.println((b+a)%b);
  return((b+a)%b);
 }
 
}  


private void scramble(int scrambleVector[],int size)
{
  int rnd1,rnd2,tmp,i;
  for(i=0;i<size;i++)
  {
   rnd1             =((int) MAFRA_Random.nextUniformLong(0,size));
   rnd2             =((int) MAFRA_Random.nextUniformLong(0,size));
   tmp              = scrambleVector[rnd1];
   scrambleVector[rnd1] = scrambleVector[rnd2];
   scrambleVector[rnd2] = tmp;
  }
}



public void setErrorThreshold(double aE)
{
 errorThreshold = aE;
}

public double getErrorThreshold()
{
 return errorThreshold;
}


public void setTemperature(double aT)
{
 temperature = aT;
}

public void setK(double aK)
{
 K=aK;
}

public double getTemperature()
{
 return temperature;
}

public double getK()
{
 return K;
}


private void clearForbidTable()
{
 int i;
 
 for(i=0;i<numIt;i++)
 {
  forbidTable[0][i] = -1;
  forbidTable[1][i] = -1;
 }
 fPos = 0;
}


private void forbid(int a, int b)
{
 forbidTable[0][fPos]=a;
 forbidTable[1][fPos]=b;
 //System.out.println("Forbidding:"+a+","+b);
 fPos++;
}

private boolean forbiden(int a, int b)
{
 int i=0;
 boolean result=false;
 do
 {
  result= (forbidTable[0][i]==a)&&(forbidTable[1][i]==b);
  i++;
 } while((!result) && (i<fPos));
 /*if (result)
  System.out.println("Forbidden:"+a+","+b);*/
 return result;
}

 /* The following methods are here in order to complete the Abstract part of the superclass */
 public Individual individualLocalSearch(Individual parent1, double temperature, double sigma, double popSize,FitnessVisitor myFitnessVisitor)
    {
     return null;
    
    }
 public Individual individualLocalSearch(Individual parent1, double temperature, double K,double distances[][],FitnessVisitor myFitnessVisitor)
    {
      return null;
    }
 public Individual individualLocalSearch(Individual parent1, double temperature,double distances[][],FitnessVisitor myFitnessVisitor)
    {
      return null;
    }


 
String parseLeftPart(String Move)
{
 String leftPart;
 int    index;
 
 index = Move.lastIndexOf("-->");
 leftPart = Move.substring(0,index);
 return leftPart;

}


 
String parseRightPart(String Move)
{
 String rightPart;
 int    index;
 
 index = Move.lastIndexOf("-->");
 rightPart = Move.substring(index+3);
 return rightPart;

}


}
