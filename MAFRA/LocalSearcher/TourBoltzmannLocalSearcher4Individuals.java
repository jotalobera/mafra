


package MAFRA.LocalSearcher;


import MAFRA.Population_Individual.*;
import MAFRA.Visitor.*;
import MAFRA.Util.*;


/** @version Memetic Algorithms Framework - V 1.0 - September 1999
    @author  Natalio Krasnogor
    */
public class TourBoltzmannLocalSearcher4Individuals extends AbstractTourLocalSearcher{


 public Individual individualLocalSearch(Individual parent1, double temperature,long distances[][],FitnessVisitor aFitnessVisitor)
 
    {


      long    rndMut; 
      double  previousFitness;
      double  newFitness;
      double  K=0.1;
      double  threshold;
      double  deltaE;
      double  rndNumber;
      int     numberChanges;
      int     numberConsecutiveChanges;
      int     i;
      int     j;
      TourIndividual tmpIndi;       
      int inversionSize;
      double size;




      tmpIndi         = (TourIndividual)(parent1.copyIndividual());
      size            = tmpIndi.getSize();
      previousFitness = tmpIndi.getFitness();

    

        numberChanges =(int) MAFRA_Random.nextUniformLong(1,(int)tmpIndi.getSize());
	numberConsecutiveChanges = (int) (tmpIndi.getSize()/numberChanges);
	System.out.println("#C="+numberChanges+", #CC"+numberConsecutiveChanges);
      //  numberChanges =(int)tmpIndi.getSize()-1;
      // numberChanges = (int)Math.ceil(size/inversionSize)-1;
      //inversionSize = (int)(MAFRA_Random.nextUniformLong(2,(int)(size)-2));
      //numberChanges = (int)(size-inversionSize)-1;
      for (i=0;i<numberChanges;i++)
	{
	  for(j=0;j<numberConsecutiveChanges;j++)
	    {
	      //  twoChangeAtI(tmpIndi,distances,i);
	      // cityInsertion(tmpIndi,distances); /* GOOD ONE, 31931 */
	       twoChange(tmpIndi,distances);
	      //  twoChange(tmpIndi,distances,i,inversionSize);
	    }
	  tmpIndi.acceptVisitor(aFitnessVisitor);
	  newFitness = tmpIndi.getFitness();
	  /* Boltzman acceptance */
	  if (Math.abs(previousFitness)<=Math.abs(newFitness))
	    {
	      deltaE    =  Math.abs(newFitness) - Math.abs(previousFitness) ;
	      threshold = Math.exp(-K*deltaE/temperature);
	      rndNumber = MAFRA_Random.nextUniformDouble(0.0,1.0);
	      // System.out.println("deltaE="+deltaE+",threshold="+threshold+", temperature="+temperature+", rndNumber="+rndNumber);
	      if(rndNumber<threshold)
		{
		  parent1.setChromosome((int[])(tmpIndi.getChromosome()));
		  parent1.setFitness(newFitness);
		  //  System.out.println("Accepting worst");
		  previousFitness = newFitness;
		}
	      else
		{
		  //  System.out.println("Rejecting worst");
		  tmpIndi.setChromosome((int[])parent1.getChromosome());
		  tmpIndi.setFitness(parent1.getFitness());
		  previousFitness = parent1.getFitness();
		}
	      
	    }
	  else
	    { /* accept configuration because it is better */
	      parent1.setChromosome((int [])(tmpIndi.getChromosome()));
	      parent1.setFitness(newFitness);
	      // System.out.println("Accepting Better");
	      previousFitness = newFitness;
	    }
	}
      
      return parent1;
      
    }


public Individual individualLocalSearch(Individual parent1, double temperature, double K, double distances[][],FitnessVisitor aFitnessVisitor)
    {
	/* not implemented */
	return(null);
    }



public Individual individualLocalSearch(Individual parent1, double temperature, double sigma,double popSize, FitnessVisitor aFitnessVisitor)
    {
	/* not implemented */
	return(null);
    }


public Individual individualLocalSearch(Individual parent1, double temperature, double distances[][],FitnessVisitor aFitnessVisitor)
    {
	/* not implemented */
	return(null);
    }



 private void cityInsertion(TourIndividual parent1, long dist[][])
   {
      int   c1[];
      int   rndPoint1;
      int   rndPoint2;
      int   tmp; 
      int   i;
      double dPlus  = 0.0;
      double dMinus = 0.0;
      
       

      c1       = (int[])(parent1.getChromosome());
      rndPoint1 = (int) MAFRA_Random.nextUniformLong(1,(long)(parent1.getSize()-1));
      rndPoint2 = (int) MAFRA_Random.nextUniformLong(1,(long)(parent1.getSize()-1));

      if(rndPoint2<rndPoint1)
	{
	  tmp       = rndPoint1;
	  rndPoint1 = rndPoint2;
	  rndPoint2 = tmp;
	}

      	
      tmp = c1[rndPoint2];
      for(i=rndPoint2;i>rndPoint1;i--)
	{
	  c1[i] = c1[i-1];
	}
      c1[rndPoint1]=tmp;


      parent1.setChromosome(c1);



   }

 private void twoChange(TourIndividual indi, long dist[][])
   {
     int startInversion;
     int endInversion;
     int size;
     int tmp;
     int c[];
     int i;
     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion =(int)MAFRA_Random.nextUniformLong(0,size-2);
     endInversion =(int)MAFRA_Random.nextUniformLong(0,size);
     //     endInversion=startInversion;
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     for (i=startInversion;i<=endInversion;i++)
       {
	 tmp=c[endInversion-i];
	 c[endInversion-i]=c[i];
	 c[i]=tmp;
       }


     indi.setChromosome(c);
   }



 private void twoChangeAtI(TourIndividual indi, long dist[][],int i)
   {
     int startInversion;
     int endInversion;
     int size;
     int tmp;
     int c[];

     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion = i;
     endInversion   = i+1;
     if(endInversion>size) endInversion=0;
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }


     for (i=startInversion;i<=endInversion;i++)
       {
	 tmp=c[endInversion-i];
	 c[endInversion-i]=c[i];
	 c[i]=tmp;
       }

     indi.setChromosome(c);
   }


 private void twoChange(TourIndividual indi, long dist[][],int i,int inversionSize)
   {
     int startInversion;
     int endInversion;
     int size;
     int tmp;
     int c[];

     double oldDs;
     double oldDe;
     double newDs;
     double newDe;

     size = (int)indi.getSize() - 1;
     c    = (int[])indi.getChromosome();
     startInversion = i;
     endInversion   = i+inversionSize;

     if(endInversion>size) endInversion=0;
     if(endInversion==startInversion)
       {
	 endInversion = endInversion+1;
	 if (endInversion>=size) endInversion=0;
       }
     if (endInversion<startInversion)
       {
	 tmp            = startInversion;
	 startInversion = endInversion;
	 endInversion   = tmp;
       }

     // System.out.println("Inverting from "+ startInversion + " to "+endInversion);
     for (i=startInversion;i<=endInversion;i++)
       {
	 tmp=c[endInversion-i];
	 c[endInversion-i]=c[i];
	 c[i]=tmp;
       }

     indi.setChromosome(c);
   }




}
